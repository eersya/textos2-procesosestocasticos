\babel@toc {spanish}{}
\contentsline {chapter}{\hbox to\@tempdima {\hfil } Prefacio}{\es@scroman {v}}
\contentsline {chapter}{\numberline {1}Generalidades sobre procesos estoc\IeC {\'a}sticos}{1}
\contentsline {section}{\numberline {1.1}Procesos equivalentes medibles}{2}
\contentsline {section}{\numberline {1.2}Filtraciones y tiempos de paro}{7}
\contentsline {section}{\numberline {1.3}La construcci\IeC {\'o}n de Kolmogorov}{17}
\contentsline {section}{\numberline {1.4}Procesos continuos}{22}
\contentsline {subsection}{\numberline {1.4.1}El espacio de Wiener}{22}
\contentsline {subsection}{\numberline {1.4.2}El criterio de continuidad de Kolmogorov}{25}
\contentsline {subsection}{\numberline {1.4.3}Familias uniformemente tensas de probabilidades y convergencia d\IeC {\'e}bil en el espacio de Wiener}{31}
\contentsline {chapter}{\numberline {2}Martingalas}{37}
\contentsline {section}{\numberline {2.1}Propiedades generales}{37}
\contentsline {section}{\numberline {2.2}Martingalas con tiempo discreto}{43}
\contentsline {subsection}{\numberline {2.2.1}El teorema de paro de Doob}{43}
\contentsline {subsection}{\numberline {2.2.2}Desigualdades maximales}{48}
\contentsline {subsection}{\numberline {2.2.3}Teorema de convergencia}{52}
\contentsline {section}{\numberline {2.3}Martingalas con tiempo continuo}{67}
\contentsline {subsection}{\numberline {2.3.1}Desigualdades maximales y el teorema de convergencia}{67}
\contentsline {subsection}{\numberline {2.3.2}Martingalas uniformemente integrables y el teorema de paro de Doob}{69}
\contentsline {subsection}{\numberline {2.3.3}Existencia de la versi\IeC {\'o}n c\IeC {\`a}dl\IeC {\`a}g de una martingala}{76}
\contentsline {section}{\numberline {2.4}Martingalas continuas}{78}
\contentsline {subsection}{\numberline {2.4.1}Martingalas locales y la descomposici\IeC {\'o}n de Doob-Meyer}{78}
\contentsline {subsection}{\numberline {2.4.2}Martingalas cuadrado integrables}{93}
\contentsline {section}{\numberline {2.5}Martingalas en espacios de Hilbert}{98}
\contentsline {chapter}{\numberline {3}El movimiento Browniano}{105}
\contentsline {section}{\numberline {3.1}Procesos Gaussianos y el Movimiento Browniano}{106}
\contentsline {subsection}{\numberline {3.1.1}Procesos Gaussianos. Construcci\IeC {\'o}n y ejemplos}{106}
\contentsline {subsubsection}{\itshape Ejemplos de procesos Gaussianos.}{108}
\contentsline {subsection}{\numberline {3.1.2}Caracterizaciones del Movimiento Browniano}{115}
\contentsline {section}{\numberline {3.2}La medida de Wiener}{137}
\contentsline {subsection}{\numberline {3.2.1}La medida de Wiener sobre el espacio de Wiener}{137}
\contentsline {subsection}{\numberline {3.2.2}Procesos isonormales y la integral de Wiener}{145}
\contentsline {subsection}{\numberline {3.2.3}La medida de Wiener como medida Gaussiana}{154}
\contentsline {subsection}{\numberline {3.2.4}Transformaci\IeC {\'o}n de la medida de Wiener por transformaciones lineales y translaciones}{159}
\contentsline {section}{\numberline {3.3}La propiedad fuerte de Markov del Movimiento Browniano}{175}
\contentsline {section}{\numberline {3.4}Las trayectorias del Movimiento Browniano}{181}
\contentsline {subsection}{\numberline {3.4.1}Propiedades locales de las trayectorias}{181}
\contentsline {subsection}{\numberline {3.4.2}La ley del logaritmo iterado}{188}
\contentsline {section}{\numberline {3.5}Movimiento Browniano en espacios de Hilber}{194}
\contentsline {subsection}{\numberline {3.5.1}El caso de la covarianza nuclear}{194}
\contentsline {subsection}{\numberline {3.5.2}El movimiento Browniano cil\IeC {\'\i }ndrico}{202}
\contentsline {subsection}{\numberline {3.5.3}El teorema de Fernique sobre la integrabilidad de las medidas Gaussianas}{207}
\contentsline {section}{\numberline {3.6}El movimiento Browniano Fraccionario}{210}
\contentsline {subsection}{\numberline {3.6.1}Caracterizaciones del Movimiento Browniano Fraccionario}{210}
\contentsline {subsection}{\numberline {3.6.2}La integral de Wiener fraccionaria}{220}
\contentsline {chapter}{\numberline {4}La integral estoc\IeC {\'a}stica}{229}
\contentsline {section}{\numberline {4.1}La integral estoc\IeC {\'a}stica con respecto a una semimartingala continua}{230}
\contentsline {subsection}{\numberline {4.1.1}La integral estoc\IeC {\'a}stica con respecto a una martingala continua cuadrado integrable}{230}
\contentsline {subsection}{\numberline {4.1.2}Integral estoc\IeC {\'a}stica con respecto a una semimartingala continua}{250}
\contentsline {section}{\numberline {4.2}La f\IeC {\'o}rmula de It\IeC {\^o}}{267}
\contentsline {subsection}{\numberline {4.2.1}Demostraci\IeC {\'o}n de la f\IeC {\'o}rmula de It\IeC {\^o} y ejemplos}{267}
\contentsline {subsection}{\numberline {4.2.2}Aplicaciones de la f\IeC {\'o}rmula de It\IeC {\^o}}{277}
\contentsline {section}{\numberline {4.3}Martingalas exponenciales y el teorema de Girsanov}{291}
\contentsline {section}{\numberline {4.4}La integral estoc\IeC {\'a}stica con respecto al Movimiento Browniano infinito dimensional}{299}
\contentsline {section}{\numberline {4.5}Integrales estoc\IeC {\'a}sticas m\IeC {\'u}ltiples}{310}
\contentsline {subsection}{\numberline {4.5.1}Integrales m\IeC {\'u}ltiples de Wiener-It\IeC {\^o}}{310}
\contentsline {subsection}{\numberline {4.5.2}Integrales estoc\IeC {\'a}sticas m\IeC {\'u}ltiples fraccionarias}{327}
\contentsline {chapter}{\numberline {6}Ecuaciones diferenciales estoc\IeC {\'a}sticas}{335}
\contentsline {section}{\numberline {6.1}Soluciones d\IeC {\'e}biles}{336}
\contentsline {subsection}{\numberline {6.1.1}Definici\IeC {\'o}n de las soluciones y la unicidad en trayectoria y en ley}{336}
\contentsline {subsection}{\numberline {6.1.2}Soluciones d\IeC {\'e}bilses y el problema de la martingala}{340}
\contentsline {subsection}{\numberline {6.1.3}El teorema de Yamada-Watanabe}{347}
\contentsline {section}{\numberline {6.2}Teoremas de existencia y unicidad}{354}
\contentsline {subsection}{\numberline {6.2.1}Teoremas de existencia}{354}
\contentsline {subsection}{\numberline {6.2.2}Teoremas de unicidad en trayectoria}{361}
\contentsline {subsection}{\numberline {6.2.3}Existencia y unicidad en ley usando el teorema de Girsanov}{370}
\contentsline {section}{\numberline {6.3}Ecuaciones de It\^{o} con coeficientes de Lipschitz}{374}
\contentsline {subsection}{\numberline {6.3.1}Existencia, unicidad y la propiedad de Markov de las soluciones}{374}
\contentsline {subsection}{\numberline {6.3.2}Dependencia continua con respecto a las condiciones iniciales y a los coeficientes}{395}
\contentsline {subsection}{\numberline {6.3.3}Diferenciabilidad con respecto a las condiciones iniciales}{401}
\contentsline {section}{\numberline {6.4}Ecuaciones de It\^{o} afines}{412}

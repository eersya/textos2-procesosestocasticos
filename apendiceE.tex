\setcounter{chapter}{4}
\chapter[Medidas Gaussianas]{Medidas Gaussianas en espacios de Hilbert}\label{E1}

\begin{definicion}\label{E:D1} Sea E un espacio de Banach separable y sea $F \in \cs{P}_{r}(E).$

 Se dice que $F$ es una {\it medida Gaussiana} si cada $ h \in E^{*},$ considerada como una variable aleatoria definida sobre el espacio de probabilidad $(E, \cs{B}(E),F)$ y con valores reales, tiene distribución normal, es decir $F \circ h^{-1} \in N(m_{h}, \sigma_{h}^{2}).$
 
Si para cada $h \in E^{*} ,\   F \circ h^{-1}$ es simétrica entonces se dice que $F$ es {\it simétrica.}

Sea $H$ un espacio de Hilbert real y separable. Entonces $F \in \cs{P}_{r}(H)$ es {\it Gaussiana} si para cada $h \in H$ se tiene que la variable aleatoria 

$ \langle h, \cdot \rangle \colon (H, \cs{B}(H),F) \to R$ tiene distribución normal. Para cada medida Gaussiana $F,$ tienen sentido las siguientes funcionales.

\begin{equation}\label{eq:e.1}
\varphi_{1} \colon H \to R, \   \varphi_{1}(h)= \int_{H} \langle h,x \rangle dF(x) 
\end{equation}

\begin{equation}\label{eq:e.2}
\varphi_{2} \colon H \times H \to R, \   \varphi_{2}(h_{1},h_{2}) = \int_{H} \langle h_{1},x \rangle \langle h_{2},x \rangle dF(x).
\end{equation}
\end{definicion}

Ahora veremos que para cada medida Gaussiana $F$ siempre existen la media y la covarianza. Para esto necesitamos el siguiente resultado general:

\begin{prop}\label{E:P2} Sea $F \in \cs{P}_{r}(H)$ tal que para algún entero no negativo $k$ se cumple

\begin{equation}\label{eq:e.3}
\int_{H} \abs{\langle z, x \rangle}^{k}\    dF(x) < \infty, \hspace{8pt} \forall z \in H.
\end{equation}

Entonces la funcional $\varphi_{k} \colon H^{k} \to R$ definida como
\begin{equation}\label{eq:e.4}
\varphi_{k} (h_{1}, \dots , h_{k}) = \int_{H} \langle h_{1}, x \rangle \cdots \langle h_{k}, x \rangle\   dF(x)
\end{equation}
es k-lineal, simétrico y continuo.
\end{prop}

\begin{proof}[\bf Demostración]

Sea

$$U_{n}= \left\lbrace z \in H : \int_{H} \abs{ \langle z,x \rangle }^{k} \   dF(x) \leq n \right\rbrace .$$

Es claro que $H= \mathop{\cup}\limits_{n=1}^{\infty} U_{n}$ y por lo tanto del teorema de categoría de Baire existe $n_{0}$ tal que $\overset{\circ}{U_{n_{0}}} \neq \emptyset$ lo que significa que existe $z_{0} \in U_{0}$ y $r_{0}>0$ tales que $\bar{B}(z_{0},r_{0}) \subset U_{n_{0}}.$ En particular resulta que

$$ \int_{H} \abs{ \langle z_{0}+y,x \rangle}^{k}\    dF(x) \leq n_{0},\hspace{8pt}  \forall y \in \bar{B}(0,r_{0}).$$

Usando la desigualdad $(a+b)^{k} \leq 2^{k-1} (a^{k} + b^{k})$ si $a,b \geq 0,$ resulta que para cada $y \in \bar{B}(0, r_{0})$ se cumple

$$\int_{H} \abs{ \langle y,x \rangle}^{k} \  dF(x) \leq 2^{k-1} \int_{H} \abs{ \langle z_{0}+y,x \rangle}^{k} \  dF(x)$$

$$+ 2^{k-1} \int_{H} \abs{ \langle z_{0},x \rangle }^{k}\  dF(x) \leq 2^{k}n_{0}.$$

Para $z \in H , \   z \neq 0,$ apliquemos la desigualdad anterior cuando $y=r_{0} z / \norm{z} .$

Se obtiene

$$ \int_{H} \abs{ \langle z,x \rangle}^{k} \   dF(x) \leq 2^{k} n_{0} \norm{z}^{k}r_{0}^{-k},$$

lo que implica (usando la desigualdad elemental $ \abs{ a_{1} \cdots a_{k}} \leq \abs{a_{1}}^{k} + \cdots + \abs{a_{k}}^{k},$ para $a_{i} \in R)$ la continuidad de $\varphi_{k}.$ La simetría y la $k$-linealidad de $\varphi_{k}$ son inmediatas.
\end{proof}

\begin{coro}\label{E:C3} Si $F \in \cs{P}_{r}(H)$ es Gaussiana, entonces existe la media $E(F)$ y la covarianza $C_{F}.$
\end{coro}

\begin{proof}[\bf Demostración] La afirmación del corolario es una consecuencia inmediata del teorema de representación de Riesz aplicado a la funcional lineal continua $\varphi_{1}$ y a la funcional bilineal, continua y simétrico (por la Proposición \ref{E:P2})

$$\tilde{\varphi}_{2}(h_{1},h_{2})=\varphi_{2}(h_{1},h_{2})- \langle E(F),h_{1} \rangle \   \langle E(F), h_{2} \rangle , \hspace{8pt} h_{1}, h_{2} \in H $$

\end{proof}

\begin{prop}\label{E:P4} Si $F \in \cs{P}_{r}(H)$ es Gaussiana entonces su función característica tiene la forma

\begin{equation}\label{eq:e.5}
\varphi_{F}(t)= e^{i \langle t, E(F) \rangle - \frac{1}{2} \langle C_{F}t,t \rangle}, \hspace{8pt} \forall t\in H.
\end{equation}


En particular resulta que $F$ está determinada únicamente por $E(F)$ y $C_{F}.$
\end{prop}

\begin{proof}[\bf Demostración] Para cada $t \in H$ la variable aleatoria

$$  \langle t, \cdot \rangle \in N( \langle t, E(F) \rangle , \   \langle C_{F}t,t \rangle)$$

(por la definición de $E(F)$ y $C_{F}).$

\end{proof}

\begin{teo}\label{E:T5} Sea $F \in \cs{P}_{r}(H)$ una medida Gaussiana de media $E(F)$ y covarianza $C_{F}.$ Entonces $C_{F} \in L_{1}(H)$ y en particular resulta

\begin{equation}\label{eq:e.6}
\int_{H} \norm{x}^{2} \   dF(x) < \infty.
\end{equation} 
\end{teo}

\begin{proof}[\bf Demostración] Sin perdida de generalidad podemos suponer que $E(F)=0.$ Entonces por (\ref{eq:e.5}) tenemos la igualdad

\begin{equation}\label{eq:e.7}
\varphi_{F}(t)= e^{\frac{1}{2}\langle C_{F},t \rangle}, \hspace{8pt} \forall t \in H.
\end{equation}

Usando el razonamiento de la demostración del teorema de Bochner-Minlos-Sazonov (Teorema \ref{D:T6}) se obtiene

\begin{equation}\label{eq:e.8}
1-\varphi_{F}(t)= - \frac{1}{2} \langle S^{R} t, t \rangle + 2F(B(0,R)^{c}),
\end{equation}

donde $S^{R} \in L_{1}(H)$ está definido por

\begin{equation}\label{eq:e.9}
\langle S^{R}t,t \rangle= \int_{B(0,R)} \langle t,x \rangle^{2} \   dF(x).
\end{equation}

Luego

\begin{equation}\label{eq:e.10}
1-e^{-\frac{1}{2} \langle C_{F}t,t \rangle} \leq \frac{1}{2} \langle S^{R}t,t \rangle + 2 F(B(0,R)^{c}), \hspace{8pt} \forall \    t\in H, \   R>0
\end{equation}

Es suficiente ver que existe una constante $\beta >0$ tal que 

\begin{equation}\label{eq:e.11}
\langle S^{R}t,t \rangle \leq 1 \Rightarrow \langle C_{F}t,t \rangle \leq \beta,
\end{equation}

(de hecho esto implicará que $C_{F} \leq \beta S^{R}$ y como consecuencia $C_{F} \in L_{1}(H)).$
Sea $t \in H$ tal que $\langle S^{R}t,t \rangle \leq 1.$ Entonces (\ref{eq:e.10}) da como resultado que

\begin{equation}\label{eq:e.12}
e^{\frac{1}{2} \langle C_{F}t,t \rangle} \leq \left[ \frac{1}{2} - 2F(B(0,R)^{c}) \right] ^{-1}.
\end{equation}

Ahora si se escoge $R$ con la propiedad de que $F(B(0,R)^{c}) < \frac{1}{4}$ entonces la desigualdad (\ref{eq:e.12}) implicará (\ref{eq:e.11}). La relación (\ref{eq:e.6}) es ahora una consecuencia inmediata de la Proposición (\ref{D:P3})-(\ref{PD3 (b)}).


El resultado que sigue demuestra que la familia de las medidas Gaussianas no es vacía.
\end{proof}

\begin{teo}\label{E:T6} (existencia de las medidas gaussianas). Sean $ m \in H$ y $ Q \in L_{1}(H),$ $Q=Q^{*} \geq 0.$ Entonces existe una y sólo una medida gaussiana $G \in \cs{P}_{r}(H)$ tal que

\begin{equation}\label{eq:e.13}
 \varphi_{G}(t)=e^{i \langle m,t \rangle - \frac{1}{2} \langle Qt,t \rangle}, \hspace{8pt} \forall t \in H.
\end{equation}
\end{teo}

\begin{proof}
Sea $\{X_{n}\}_{n \geq 1}$ una sucesión de variables aleatorias reales independientes tal que $X_{n} \in N(0,1).$ Sea $\{e_{n}\}_{n \geq 1}$ el sistema ortonormal formado por los vectores propios de Q (véase Teorema \ref{C:T6}) y sea $\{\lambda_{n}\}_{n \geq 1}$ los valores propios correspondientes. Definimos la variable aleatoria con valores en $H$ por:

\begin{equation}\label{eq:e.14}
X= m+ \mathop{\sum}\limits_{n=1}^{\infty} \sqrt{\lambda_{n}}X_{n}e_{n}.
\end{equation}

Es claro que la serie que aparece al lado derecho de (\ref{eq:e.14}) converge en $L_{2}(\Omega)$ y {\it c.s.} (véase Teorema \ref{D:T11}). Ahora usando la independencia de $X_{n}$ y el teorema de convergencia dominada se tiene fácilmente

$$ \varphi_{X}(t)= e^{i \langle t, m \rangle - \frac{1}{2} \langle Qt, t \rangle}, $$

es decir, $P \circ X^{-1}$ es Gaussiana de media $E(F)$ y covarianza $C_{F}.$
\end{proof}

\begin{observacion}\label{E:07} El teorema anterior nos permite denotar por $N(m,Q)$ a la única $G \in \cs{P}_{r}(H)$ Gaussiana que satisface (\ref{eq:e.13}).
\end{observacion}

\begin{prop}\label{E:P8} Si dim $H = \infty$ entonces no existe ninguna $F \in \cs{P}_{r}(H)$ tal que 

\begin{equation}\label{eq:e.15}
\varphi_{F}(t)= e^{-\frac{\norm{t}^{2}}{2}},
\end{equation}

es decir, no existe $N(0,1)$ (recordemos que $I \notin L_{1}(H)).$
\end{prop}

\begin{proof}

Supongamos que existe $F$ tal que (\ref{eq:e.15}) se cumple. En particular de (\ref{eq:e.15}) resulta que para cada $t \in H$ la variable aleatoria $ \langle t, \cdot \rangle \in N(0, \norm{t}^{2}) = N(0, \langle It,t \rangle )$ lo que significaría  que $F$ es Gaussiana $N(0,1)$ y por el Teorema \ref{E:T5} resultará que $I \in L_{1}(H)$ lo que es una contradicción.
\end{proof}

Del Teorema \ref{D:T11} se obtiene en particular la siguiente proposición.

\begin{prop}\label{E:P9} Sea $f_{1}, \dots , f_{n}, \dots , $ una sucesión de variables aleatorias con valores en $H$ independientes y con distribución $N(0,Q)$ y sea $S_{n}= f_{1} + \cdots + f_{n}.$

Entonces las siguientes condiciones son equivalentes:

\begin{enumerate}[\rm(a)]

\item $S_{n}$ converge en probabilidad.

\item $S_{n}$ converge {\it c.s..}

\item $S_{n}$ converge en media cuadrática.

\item $S_{n}$ converge en distribución.
\end{enumerate}
\end{prop}
\setcounter{chapter}{5}
\chapter{La probabilidad condicional regular}\label{F1}

\begin{definicion}\label{F:D1}
Un espacio medible $(\Omega,\mathcal{F})$ es un {\itshape espacio de Borel} (o {\itshape espacio estándar}) si existe $F\in\mathcal{B}(R)$ y una función biunívoca $\varphi\colon(\Omega,\mathcal{F})\to(F,\mathcal{B}(F))$ tal que $\varphi$ y $\varphi^{-1}$ son medibles (en este caso se dice que $\varphi$ es {\itshape bimedible}).
\end{definicion}

\begin{proble}\label{F:Pr2}
Probar que $(R^d,\mathcal{B}(R^d))$, $(R^\infty,\mathcal{B}(R^\infty))$ y en general $(E,\mathcal{B}(E))$, donde $E$ es un espacio métrico separable y completo (es decir espacio polaco), son espacios de Borel.
\end{proble}

\begin{teo}\label{F:T3}
(Teorema de desintegración). Sean $(\Omega,\mathcal{F})$ un espacio medible, $(E,\mathcal{E})$ un espacio de Borel y $P$ una probabilidad sobre $\mathcal{F}\otimes\mathcal{E}$. Si $P_1=P\circ\pi_1^{-1}$, donde $\pi_1\colon \Omega\times E\to E$ es la proyección $\pi_1((\omega,x))=\omega$, entonces existe una probabilidad de transición $Q_1(\omega,A)$ de $(\Omega,\mathcal{F})$ a $(E,\mathcal{E})$ tal que $P$ se desintegra con respecto a $P_1$, es decir $P=P_1\otimes Q_1$ (véase la Sección \ref{subsection5.2.1} para detalles con respecto a las funciones de trancisión).
\end{teo}

\begin{proof}
{\itshape Caso 1}. Sea $E=R$, $\cs{E}=\cs{B}(R)$. Se sabe que dar una probabilidad sobre $\cs{B}(R)$ equivale a dar su función de distribución. Por lo tanto construir una probabilidad de transición $Q_1(\omega,A)$, $\omega\in\Omega$, $A\in\cs{B}(R)$ equivale a construir una función $F(\omega,x)\colon\Omega\times R\to[0,1]$ tal que $F(\cdot,x)$ sea $\cs{F}$-medible para cada $x\in R$ y $F(\omega,.)$ sea una función de distribución para cada $\omega\in\Omega$.

Si $r\in Q$ consideramos la esperanza condicional
\begin{equation*}
\begin{gathered}
E\left[1_{\Omega\times(-\infty,r)}|\pi_1^{-1}(\cs{F})\right].
\end{gathered}
\end{equation*}

Escogemos una función $\tilde{F}(\omega,r)$ que es $\cs{F}$-medible en $\omega$ y tal que
\begin{equation}\label{eq:f.1}
\begin{gathered}
E\left[1_{\Omega\times(-\infty,r)}|\pi_1^{-1}(\cs{F})\right]=\tilde{F}(\pi_1,r)\quad P\text{-c.s.}.
\end{gathered}
\end{equation}

De esta manera obtenemos una función $\tilde{F}(\omega,r)\colon\Omega\times Q\to[0,1]$ que satisface (\ref{eq:f.1}) y la cual es $\cs{F}$-medible en $\omega$.

Es claro que si $r<s$ entonces
\begin{equation*}
\begin{gathered}
\tilde{F}(\pi_1,r)\leq\tilde{F}(\pi_1,s)\quad P\text{-c.s.},
\end{gathered}
\end{equation*}
lo que implica que $\tilde{F}(\omega,r)\leq\tilde{F}(\omega,s)$ $P_1$-c.s.. Si hacemos
\begin{equation*}
\begin{gathered}
M_{r,s}=\left\{\omega\in\Omega:\tilde{F}(\omega,r)>\tilde{F}(\Omega,s)\right\}
\end{gathered}
\end{equation*}
entonces se obtiene el conjunto $M=\displaystyle{\bigcup_{r<s,\,\,\,r,s\in Q}M_{r,s}}$ donde $\tilde{F}(\omega,r)$ no es creciente. Como $P_1(M_{r,s})=0$ para cada $r<s$ resulta que $P_1(M)=0$.

Sea
\begin{equation*}
\begin{gathered}
N_s=\left\{\omega\in\Omega:\lim_{r\nearrow s}\tilde{F}(\omega,r)\neq\tilde{F}(\omega,s)\right\},\quad N=\bigcup_{s\in Q}N_s.
\end{gathered}
\end{equation*}

Como
\begin{equation*}
\begin{gathered}
\tilde{F}(\pi_1,r)\nearrow\tilde{F}(\pi_1,s)\quad P\text{-c.s.},
\end{gathered}
\end{equation*}
resulta que
\begin{equation*}
\begin{gathered}
\tilde{F}(\omega,r)\nearrow\tilde{F}(\omega,s)\quad P_1\text{-c.s.},
\end{gathered}
\end{equation*}
de donde $P_1(N_s)=0$ y por lo tanto $P_1(N)=0$.

De manera semejante se demuestra que los conjuntos
\begin{equation*}
\begin{gathered}
L_1=\left\{\omega\in\Omega:\lim_{r\nearrow\infty}\tilde{F}(\omega,r)=1\right\},
\end{gathered}
\end{equation*}
\begin{equation*}
\begin{gathered}
L_2=\left\{\omega\in\Omega:\lim_{r\searrow-\infty}\tilde{F}(\omega,r)=0\right\},
\end{gathered}
\end{equation*}
tienen $P_1$-medida cero.

Resulta que $\Gamma=M\cup N\cup L\cup L_1\cup L_2\cup$ es de $P_1$-medida cero y para $\omega\notin\Gamma$ la función $\tilde{F}(\omega,\cdot)$ satisface las siguientes condiciones:
\begin{enumerate}[(a)]
	\item Es no decreciente.
	\item Es continua por la izquierda.
	\item $ \mathop{\lim}\limits_{r\nearrow \infty} \tilde{F}(\omega,r)=1$, $\mathop{\lim}\limits_{r\searrow -\infty} \tilde{F}(\omega,r)=0$.
\end{enumerate}

Definamos $F\colon\Omega\times R\to[0,1]$ por

\begin{equation*}
F(\omega,x)=
\begin{cases}
	\mathop{\sup}\limits_{r \in Q,r<x} \tilde{F}(\omega,r) \quad si \quad \omega \notin \Gamma,\\
	G \quad si \quad \omega \in \Gamma\\
\end{cases}
\end{equation*}

donde G es una función de distribución arbitraria.\\
No es difícil ver que $F(\omega,x)$ es $\cs{F}$-medible en $\omega$ y es función de distribución en x.\\
Sea $Q_{1}(\omega,A)$ la probabilidad (de transición) construida a partir de $F$. Mostraremos que $P= P_{1} \otimes Q_{1}$.\\
Por el teorema de las clases monótonas es suficiente probar la igualdad sobre los conjuntos de la forma $A \times (-\infty, r)$, $A \in \cs{F}$, $r \in Q$.\\
En este caso se tiene que:

$$ P(A \times (-\infty, r)) = P( \{A \times R \} \cap \{ \Omega \times (-\infty, r)\}) $$

$$ = \mathop{\int}\limits_{A \times R} 1_{\Omega \times (-\infty,r)} dP = \mathop{\int}\limits_{A \times R} E\left[1_{\Omega\times(-\infty,r)}|\pi_1^{-1}(\cs{F})\right] dP $$

$$ = \mathop{\int}\limits_{A \times R} F(\pi_{1},r) dP = \mathop{\int}\limits_{A} \tilde{F}(\omega,r) dP_{1}(\omega) $$

$$ = \mathop{\int}\limits_{A} Q_{1}(\omega, (-\infty,r)) dP_{1}(\omega) = (P_{1} \otimes Q_{1})(A \times(-\infty,r)) $$

{\itshape Caso 2}. Sea $E \in \cs{B}(R)$, $\cs{E} = \cs{B}(E)$. Definamos la probabilidad $\tilde{P}$ sobre $\cs{F} \otimes \cs{B}(R)$ como $\tilde{P}(B) = P(B \cap ( \Omega \times E))$.\\
Se tiene $\tilde{P}(A \times R) = P(A \times E) = P_{1}(A)$ y por el caso 1 resulta que existe una probabilidad de transición $\tilde{Q}_{1}(\omega, A)$, $\omega \in \Omega$, $A \in \cs{B}(R)$ tal que 

$$ \tilde{P}(B) = \int \tilde{Q}_{1}(\omega, B_{\omega}) dP_{1}(\omega), \quad \forall B \in \cs{F} \otimes \cs{B}(R) $$

donde $B_{\omega}$ es la sección de B en $\omega$.\\
Como

$$ 1 = P(\Omega \times E) = \tilde{P}(\Omega \times E) = \int \tilde{Q}_{1}(\omega,E) dP_{1}(\omega), $$

y $0 \leq \tilde{Q}_{1} \leq 1$ se tiene que $\tilde{Q}_{1} ( \omega , E ) = 1$ es $P_{1}$\text{-c.s.} y como consecuencia existe $N \in \cs{F}$ tal que $P_{1}(N)=0$ y $\tilde{Q_{1}}(\omega,E)=1$ si $\omega \notin N$.\\
Entonces

\begin{equation*}
Q_{1}(\omega,A)=
\begin{cases}
	\tilde{Q}_{1}(\omega,A) \quad si \quad \omega \notin N,\\
	\epsilon_{0}(A) \quad si \quad \omega \in N\\
\end{cases}
\end{equation*}

es probabilidad de transición que satisface $P = P_{1} \otimes Q_{1}$.

{\itshape Caso 3}. Sea $(E,\cs{E})$ un espacio de Borel, $F \in \cs{B}(R)$ y sea la aplicación $\varphi \colon (E,\cs{E}) \to (F, \cs{B}(F))$ biunívoca y bimedible.\\
Sea la aplicación medible

$$ (\omega,x) \to (\omega, \varphi(x)) \colon \Omega \times E \to \Omega \times F $$

y sea $\tilde{P}$ la imagen de $P$ por esta aplicación.\\
Debido al caso 2 existe una probabilidad de transición $\tilde{Q}_{1} \colon \Omega \times \cs{B}(F) \to \left[0,1\right]$  tal que $\tilde{P}=P_{1} \otimes \tilde{Q}_{1}$.\\
Definimos $Q_{1} \colon \Omega \times \cs{E} \to \left[0,1\right]$ por

$$ Q_{1}(\omega,A) = \tilde{Q}(\omega, \varphi(A)). $$

No es difícil ver que $Q_{1}$ satisface los requisitos.

\end{proof}

\begin{definicion}\label{F:D4}
Sea $(\Omega, \cs{F},P)$ un espacio de probabilidad y $\cs{F}_{1}$, $\cs{F}_{2}\subset \cs{F}$ dos sub-$\sigma$-álgebras.\\
Una función $Q(\omega, A) \colon \Omega \times \cs{F}_{1} \to \left[0,1\right]$ con las siguientes propiedades:
\begin{enumerate}[(i)]
	\item $Q(\omega,A)$ es una probabilidad de transición de $(\Omega,\cs{F}_{2})$ a $(\Omega,\cs{F}_{1})$.
	\item Para cada $A \in \cs{F}_{1}$, $Q(\cdot,A)$ es una versión de $P(A|\cs{F}_{2})$,
\end{enumerate}
se llama \textit{probilidad condicional regular sobre $\cs{F}_{1}$ con respecto a $\cs{F}_{2}$} (Si $\cs{F}_{1}= \cs{F}$ diremos simplemente \textit{probabilidad condicional regular con respecto a $\cs{F}_{2}$}).
\end{definicion}

La ventaja de la probabilidad condicional regular se ve en el siguiente teorema:

\begin{teo}\label{F:T5}
Sea $Q(\omega,A)$ una probabilidad regular sobre $\cs{F}_{1}$, con respecto a $\cs{F}_{2}$ y sea $X$ una variable aleatoria $\cs{F}_{1}$-medible y P-integrable.\\
Entonces
\begin{equation} \label{eq:f.2}
E \left[ X | \cs{F}_{2} \right] = \int X(\tilde{\omega}) Q(\omega,d\tilde{\omega}) \quad P\text{-c.s..}
\end{equation}
\end{teo}

\begin{proof}
La igualdad (\ref{eq:f.2}) es inmediata si $X=1_{A}$, $A \in \cs{F}_{1}$. Luego su validez en general es consecuencia del teorema de las clases monótonas.
\end{proof}

Sea $(\Omega,\cs{F},P)$ un espacio de probabilidad, $\cs{R}$ una sub $\sigma$-álgebra de $\cs{F}$, $(E,\cs{E})$ un espacio medible y $X \colon (\Omega,\cs{F},P) \to (E,\cs{E})$ una variable aleatoria.

\begin{definicion} \label{F:D6}
Llamamos \textit{probabilidad condicional regular} de $X$ dada $\cs{R}$ a una función $Q(\Omega,A) \colon \Omega \times \cs{E} \to \left[ 0,1 \right]$ tal que:
\begin{enumerate}[(a)]
\item $Q(\omega,A)$ es probabilidad de transición de $(\Omega,\cs{R})$ a $(E,\cs{E})$.
\item para cada $B \in \cs{E}$, $Q(\omega,B)=P(X^{-1}(B) | \cs{R})$ o de modo equivalente
\end{enumerate}

\begin{equation}\label{eq:f.3}
\mathop{\int}\limits_{A} Q(\omega,B) dP(\omega) = P(X^{-1}(B) \cap A),
\end{equation}

Para cada $A \in \cs{R}$ (es decir, Q es la probabilidad condicional regular sobre $X^{-1}(\cs{R})$ dado $\cs{R}$).
\end{definicion}

\begin{observacion}\label{F:O7}
Es claro que si $(E,\cs{E})=(\Omega,\cs{F})$ y $X$ es la identidad, entonces se obtiene la definición (\ref{F:D4}).
\end{observacion}

\begin{teo} \label{F:T8}
(Existencia de la probabilidad condicional regular). Sea $(\Omega,\cs{F},P)$ un espacio de probabilidad, $\cs{R}$ una sub-$\sigma$-álgebra de $\cs{F}$, $(E,\cs{E})$ un espacio de Borel y $X \colon (\Omega,\cs{F},P) \to (E,\cs{E})$ una variable aleatoria.\\
Entonces existe una probabilidad condicional de $X$ dado $\cs{R}$.
\end{teo}

\begin{proof}
Sea la aplicación medible $j \colon (\Omega, \cs{F}) \to (\Omega \times E, \cs{R} \otimes \cs{E})$ definido por $j(\omega) = (\omega, X(\omega))$ y sea $\tilde{P} = P \circ j^{-1}$. Es claro que $\tilde{P}$ es una probabilidad sobre $\cs{R} \otimes \cs{E}$ y $P|_{\cs{R}}=\tilde{P} \circ \pi_{1}^{-1}$. Como consecuencia del teorema de desintegración (\ref{F:T3}), resulta la existencia de una probabilidad de transición $Q(\omega,A)$ de $(\Omega, \cs{R})$ a $(E,\cs{E})$ tal que

\begin{equation} \label{eq:f.4}
\tilde{P}(B) = \int Q(\omega, B_{\omega}) dP(\omega), \quad \forall B \in \cs{R} \otimes \cs{E}.
\end{equation}

En particular tomando en (\ref{eq:f.4}), $B=C \times D$, $C \in \cs{R}$, $D \in \cs{E}$ y teniendo en cuenta que $j^{-1}(C \times D) = X^{-1}(D) \cap C$ se obtiene

$$ \mathop{\int}\limits_{C} Q(\omega,D) dP(\omega) = \tilde{P}(C \times D) = (P \circ j^{-1})(C \times D) = P(X^{-1}(D) \cap C), $$

Lo que prueba que Q es una probabilidad condicional regular de $X$ dado $\cs{R}$.
\end{proof}

\begin{definicion}\label{F:D9}
Un espacio medible $(\Omega,\cs{F})$ es \textit{numerablemente determinado} si existe $\cs{D} \subset \cs{F}$, $\cs{D}$ numerable, tal que cualesquiera dos probabilidades que coincidan sobre $\cs{D}$ coinciden sobre $\cs{F}$.
\end{definicion}

\begin{observacion}\label{F:O10}
Todo espacio de Borel está numerablemente determinado.
\end{observacion}

\begin{teo}\label{F:T11}
Sea $(\Omega,\cs{F})$ un espacio de Borel y $P$ una probabilidad sobre $\cs{F}$. Sea $\cs{G}$ una sub-$\sigma$-álgebra de $\cs{F}$ y sea $P(\omega,d\omega')$ una probabilidad condicional regular dado $\cs{G}$.\\
Si $\cs{H} \subset \cs{G}$ es una $\sigma$-álgebra numerablemente determinada entonces existe $N \in \cs{G}$ con $P(N)=0$ tal que para $\omega \notin N$ se tiene

$$ P(\omega,A) = 1_{A}(\omega), \quad \forall A \in \cs{H}. $$
\end{teo}

\begin{proof}
Sea $\cs{H}_{0} \subset \cs{H}$ una familia tal que cada dos probabilidades que son iguales sobre $\cs{H}_{0}$ resultan iguales sobre $\cs{H}$.\\
Es claro que si $A \in \cs{H}_{0}$ entonces existe $N_{A} \in \cs{G}$ con $P(A)=0$ tal que $P(\omega,A)=1_{A}(\omega)$ si $\omega \notin N_{A}$.\\
Hagamos $N= \bigcup \limits_{A \in \cs{H}_{0}} N_{A}$. Entonces $P(N)=0$ y para $\omega \notin N$ se tiene $P(A,\omega)=1_{A}(\omega)$ para cada $A \in \cs{H}_{0}$ y por consiguiente $P(\omega,A)=1_{A}(\omega)$ para cada $A \in \cs{H}$.
\end{proof}

\begin{coro}\label{F:C12}
Sea $(\Omega,\cs{F})$ un espacio de Borel y P una probabilidad sobre $\cs{F}$. Sea $\cs{G}$ una sub-$\sigma$-álgebra contenida en $\cs{F}$ y sea $P(\omega,d\omega ')$ una probabilidad condicional regular dado $\cs{G}$.\\
Sea $\xi \colon (\Omega,\cs{G},P) \to (S,\cs{B})$ una variable aleatoria. Supongamos que $\cs{B}$ es numerablemente determinada y $\{x\} \in \cs{B}$ para cada $x \in S$ (por ejemplo esto se cumple si $(S,\cs{B})$ es un espacio de Borel).\\
Entonces

\begin{equation}\label{eq:f.5}
P(\omega, \{\omega' \in \Omega \colon \xi(\omega')= \xi(\omega)\})=1 \quad P\text{-c.s.} \quad \omega \in \Omega.
\end{equation}
\end{coro}

\begin{proof}
Como $\cs{B}$ es numerablemente determinada existe una familia $\cs{B}_{0} \subset \cs{B}$, $\cs{B}_{0}$ numerable, tal que si dos probabilidades son iguales sobre $\cs{B}_{0}$ son iguales sobre $\cs{B}$. Sea $\cs{H}=\{\xi^{-1}(B) \colon B \in \cs{B}\}$. Es claro que $\cs{H}$ es la $\sigma$-álgebra y si se define $\cs{H}_{0}=\{\xi^{-1}(B) \colon B \in \cs{B}_{0}\}$, Entonces resulta $\cs{H}$ es numerablemente determinada por $\cs{H}_{0}$.\\
Por el teorema anterior existe $N \in \cs{G}$ con $P(N)=0$ tal que
$$ P(\omega,A)= 1_{a}(\omega), \quad \forall \omega \notin N, \quad A \in \cs{H}. $$
Tomando
$$ A_{\omega} = \{ \omega' \in \Omega \colon \xi (\omega') = \xi(\omega)\} = \xi^{-1}(\{\xi(\omega)\}) \in \cs{H}, $$
resulta que $P(\omega, A_{\omega}) = 1_{A_{\omega}}(\omega) = 1, \quad \forall \omega \notin N$.
\end{proof}

% p.641s
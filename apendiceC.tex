\setcounter{chapter}{2}
\chapter[Operadores compactos]{Operadores compactos, Hilbert-Schmidt y nucleares}\label{C1}

Sean $H_{1},\  H_{2},\  H$ espacios de Hilbert reales separables con las normas $\norm{\cdotp}_{1} = \langle \cdotp , \cdotp \rangle_{1}^{1/2},\  \norm{\cdotp}_{2}= \langle \cdotp , \cdotp \rangle_{2}$ y  $\norm{\cdotp} = \langle \cdotp , \cdotp \rangle.$ Denotemos por $L(H_{1}, H_{2})$ (simplemente $L(H)$ cuando $H_{1}=H_{2}=H$) el espacio de todos los operadores $A \colon H_{1} \to H_{2}$ lineales y acotados (continuos). Se sabe que $L(H_{1},H_{2})$ es un espacio de Banach (no separable) con respecto a la norma $\norm{A}= \mathop{\sup}\limits_{\norm{x}_{1}=1} \norm{Ax}_{2}.$

Sea $A \in L(H_{1},H_{2}).$ Denotemos por $A^{\ast} \in L(H_{2},H_{1})$ al {\it adjunto de} $A$ que satisface $ \langle Ax,y \rangle_{2} = \langle x, A^{\ast}y \rangle_{1}$ para cada $ x \in H_{1},\  y\in H_{2}$ y el {\it operador inverso} $A^{-1} \colon D(A^{-1})\subset H_{2} \to H_{1}$ definido por: $D(A^{-1}):= Im(A)=A(H_{1})$ y para $x \in Im(A)$ se define $A^{-1}x$ como el elemento de $A^{-1}(\{x\})$ de norma mínima.

Es claro que $Im(A^{-1})$ es el subespacio de $H_{1}$ ortogonal a

$$Ker(A):=\{x\in H_{1}: Ax=0\}.$$

La siguiente caracterización de las imágenes de los operadores es a menudo muy útil (véase \cite{23}-pág. 407):
\setcounter{teo}{1}
\begin{teo}\label{C:T2} Sean $A_{1} \in L(H_{1},H),\  A_{2} \in L(H_{2},H).$
\begin{enumerate}[\rm(a)]
\item Entonces $Im(A_{1}) \subset Im(A_{2})$ si y sólo si existe una constante $k>0$ tal que

$$ \norm{A_{1}^{\ast}h}_{1} \leq k \norm{A_{2}^{\ast}h}_{2},\hspace{5pt} \forall h\in H.$$

\item Además si $ \norm{A_{1}^{\ast}h}_{1} = \norm{A_{2}^{\ast}h}_{2}$ para cada $h\in H$ resulta que $Im(A_{1})=Im(A_{2})$ y $ \norm{A_{1}^{-1}h}_{1}= \norm{A_{2}^{-1}h}_{2}$ para cada $h\in Im(A_{1}).$
\end{enumerate}
\end{teo}

\begin{definicion}\label{C:D3} Un operador $A \in L(H)$ se dice que es:
\begin{enumerate}[\rm(a)]
\item {\it Autoadjunto} si $A=A^{*},$ es decir, $\langle Ax,y\rangle= <x,Ay>$ para todo $x,y \in H.$

\item {\it No-negativo} (en not. $A \geq 0$) si $\langle Ax,x \rangle \geq 0$ para cada $x \in H$ y {\it positivo} (en not. $A>0$) si $\langle Ax,x \rangle >0$ para cada $x \neq 0$. Cuando $A \geq 0$ denotaremos por $A^{\frac{1}{2}}$ a  {\it la  raíz cuadrada de} $A.$ 
\end{enumerate}
\end{definicion}

\begin{definicion}\label{C:D4} Un operador $A \colon H \to H$ lineal es {\it compacto} si la imagen de cada conjunto acotado es relativamente compacto, o equivalentemente si $x_{n}$ converge débilmente a $x$ (en not. $x_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^{w} x$) se tiene que $Ax_{n} \mathop{\longrightarrow}\limits_{n \to \infty} Ax.$ 
Denotaremos por $K(H)$ a la familia de todos los operadores compactos sobre $H.$
\end{definicion}

\begin{observacion}\label{C:O5}\begin{enumerate}[\rm(1)]\item Todo operador compacto es acotado y además si $dim(H)= \infty$ la identidad $I \colon H \to H$ no es compacto.
\item Si $A \in K(H),\  B\in L(H)$ resulta que $AB,\  BA \in K(H)$
\item $K(H)$ es un espacio cerrado de $L(H),$ es decir, si $A_{n} \to A$ en $L(H),$ $A_{n}\in K(H)$ entonces $A \in K(H).$
\item Sea $S(H)=\{A \colon H \to H :A \text{ es lineal y A(H) es finito dimensional} \};$ un operador $A\in S(H)$ se llama {\it operador de rango finito}.
\end{enumerate}
Si $A\in S(H)$ entonces $A\in K(H)$ y además $\overline{S(H)}=K(H)\  (\overline{S(H)}$ es la cerradura de $S(H)$ con la norma de $L(H)).$
\end{observacion}

\begin{teo}\label{C:T6} (véase \cite{170}). Si $A\in K(H),0\neq\   A=A^{\ast}\geq 0$ entonces $A$ tiene al menos uno y a lo más un número numerable de valores propios y $\lambda=0$ es el único punto límite posible de los valores propios.

Cada valor propio de $A$ es real y cada valor propio no nulo tiene multiplicidad finita.

Sean $\{\lambda_{n}\}_{n\geq 1}$ los valores propios no nulos donde cada $\lambda_{n}$ se toma p-veces si tiene la multiplicidad p.

Entonces $\lambda_{n} \searrow 0$ y existe un sistema ortonormal $\{e_{n}\}_{n\geq 1}$ de vectores propios que corresponden a $\{\lambda_{n}\}_{n\geq 1}$ y además el sistema formado $\{e_{n}\}_{n \geq 1}$ y una base normal del $Ker(A)$ constituye una base ortonormal para $H$ (si $A>0$ resultará que $\{e_{n}\}_{n \geq 1}$ será una base ortonormal para $H$).
Por último el operador $A$ tiene la siguiente representación:

\begin{equation}\label{eq:c.1}
Ax= \mathop{\sum}\limits_{n=1}^{\infty} \lambda_{n} \langle x,e_{n} \rangle e_{n},\   \forall x \in H.
\end{equation} 
\end{teo}
\begin{proble}\label{C:Pr7} Si $\{e_{n}\}_{n\geq 1}$ es un sistema ortonormal y $\lambda_{n} \mathop{\longrightarrow}\limits_{n\to \infty} 0$ probar que el operador definido por (\ref{eq:c.1}) es compacto.
\end{proble}

\begin{observacion}\label{C:O8} Un operador $A \in K(H), A\neq 0,$ es no-negativo (resp. positivo) si y sólo si para cada valor propio $\lambda$ se tiene que $\lambda \geq 0$ (resp. $\lambda >0).$
Un ejemplo de un operador compacto está dado por el siguiente resultado (véase \cite{116}-Ejemplo 3, pág 6).
\end{observacion}

\begin{lema}\label{C:L9} Sea $D=[a,b]$ ó $R, R_{+}$ y $ v \colon D^{2} \to R$ una función, tal que $v \in L^{2}(D^2).$ Entonces el operador $T \colon L^{2}(D) \to L^{2}(D)$ definido por 
\begin{equation}\label{eq:c.2}
(Tf)(t)= \int _{D} v(s,t) f(s) ds,
\end{equation}
es compacto, no-negativo y autoadjunto.
\end{lema}
\begin{teo}\label{C:T10} (Teorema de Mercer- ver \cite{175}). Sea $D=[a,b]$ y $v \colon D^{2} \to C,  v\neq 0,$ una aplicación continua, simétrica y no-negativa definida.

Sean $\{e_{n}\}_{n\geq 1} , \{\lambda_{n}\}_{n\geq 1}$ los vectores y los valores propios del operador $T$ definido en (\ref{eq:c.2}). Entonces
\begin{equation}\label{eq:c.3}
 \mathop{\sum}\limits_{n=1}^{\infty} \lambda_{n} < \infty,
\end{equation}
\begin{equation}\label{eq:c.4}
v(t,s)=\mathop{\sum}\limits_{n=1}^{\infty} \lambda_{n} e_{n}(s) \overline{e_{n}(t)}
\end{equation}

(la serie (\ref{eq:c.4}) es absolutamente y uniformemente convergente en $D^{2}$).
\end{teo}

\begin{definicion}\label{C:D11} Un operador $A \in L(H)$ es {\it Hilbert-Schmidt} si para alguna base ortonormal $\{e_{n}\}_{n\geq 1}$ se tiene que
\begin{equation}\label{eq:c.5}
\mathop{\sum}\limits_{n=1}^{\infty} \norm{Ae_{n}}^{2} < \infty .
\end{equation}
\end{definicion}

\begin{observacion}\label{C:O12} Si (\ref{eq:c.5}) se cumple para una base ortonormal entonces se cumplirá para toda base ortonormal.
\end{observacion}

\begin{definicion}\label{C:D13} Si $A\in L(H)$ es Hilbert-Schmidt entonces se define {\it la norma Hilbert-Schmidt de} A como
\begin{equation}\label{eq:c.6}
\norm{A}_{2}= \left( \mathop{\sum}\limits_{n=1}^{\infty} \norm{Ae_{n}}^{2} \right) ^{\frac{1}{2}} 
\end{equation}

De la Observación (\ref{C:O12}) el lado derecho de (\ref{eq:c.6}) no depende de la base $\{e_{n}\}_{n\geq 1}.$ 
Denotaremos por $L_{2}(H)$ a la clase de todos los operadores Hilbert-Schmidt $A$ sobre $H.$
\end{definicion}

Para los siguientes resultados (y también para algunos precedentes de esta sección) el lector puede consultar \cite{64}, \cite{100}, \cite{116}, \cite{170}.

\begin{prop}\label{C:P14}
\begin{enumerate}[\rm(a)] \item Si $A \in L_{2}(H)$ entonces $A^{*} \in L_{2}(H)$ y además $\norm{A}_{2}= \norm{A^{*}}_{2}.$

\item $\norm{\alpha A}_{2} = \abs{\alpha} \norm{A}_{2}, \     \alpha \in R.$
\item $\norm{A + B}_{2} \leq \norm{A}_{2} + \norm{B}_{2}.$
\item $\norm{A} \leq \norm{A}_{2}.$
\item Si $A \in L(H)$ y $B \in L_{2}(H)$ entonces $AB \in L_{2}(H)$ y 
$$\norm{AB}_{2} \leq \norm{A} \hspace{3pt} \norm{B}_{2}.$$

Si $A \in L_{2}(H)$ y $B \in L(H)$ entonces $AB \in L_{2}(H)$ y
$$ \norm{AB}_{2} \leq \norm{A}_{2} \hspace{3pt} \norm{B}.$$ 

\end{enumerate}
\end{prop}

\begin{prop}\label{C:P15} $L_{2}(H)$ es un espacio de Hilbert separable con respecto al producto interno
\begin{equation}\label{eq:c.7}
\langle \langle A,B \rangle \rangle _{2} = \mathop{\sum}\limits_{n=1}^{\infty} \langle Ae_{n},Be_{n} \rangle ,
\end{equation}
 
donde $\{e_{n}\}_{n \geq 1}$ es una base ortonormal en $H$ ({\rm de nuevo la serie del lado derecho de (\ref{eq:c.7}) no depende de la base escogida).}
\end{prop} 

\begin{proble}\label{C:Pr16} Probar que el operador dado por (\ref{eq:c.2}) pertenece a $L_{2}(L^{2}(D^{2}))$ y $\norm{T}_{2} = \norm{v}_{L^{2}(D^{2})} .$
\end{proble}

\begin{observacion}\label{C:O17}
\begin{enumerate}[\rm(a)] \item Si $A \in L_{2}(H)$ entonces $A\in K(H).$
\item Sea $A \in K(H)$ un operador como en el Teorema \ref{C:T6}. Entonces $A$ es Hilbert-Schmidt si y sólo si 
\begin{equation} \label{eq:c.8}
\mathop{\sum}\limits_{n=1}^{\infty} \lambda_{n}^{2} < \infty .
\end{equation}

En este caso se tiene la igualdad 
\begin{equation}\label{eq:c.9}
\norm{A}_{2}= \left( \mathop{\sum}\limits_{n=1}^{\infty} \lambda_{n}^{2} \right)^{\frac{1}{2}}.
\end{equation}

\end{enumerate}
\end{observacion}

\begin{definicion}\label{C:D18} Un operador $A\in K(H)$ es de {\it la clase traza (o nuclear)} si
\begin{equation}\label{eq:c.10}
\mathop{\sum}\limits_{n=1}^{\infty} \mu _{n} < \infty ,
\end{equation}

donde $\{\mu _{n}\}_{n \geq 1}$ son los valores propios del operador $( A^{*} A)^{\frac{1}{2}} .$
\end{definicion}

\begin{definicion}\label{D:C.19} Si el operador $A$ es de la clase traza se define la {\it norma traza de} $A$ por
\begin{equation}\label{eq:c.11}
\norm{A}_{1}= \mathop{\sum}\limits_{n=1}^{\infty} \mu _{n}.
\end{equation}  
\end{definicion}

Denotaremos por $L_{1}(H)$ a la clase de todos los operadores de la clase traza $A$ sobre $H.$

\begin{definicion}\label{D:C.20} Si $A \in L_{1}(H)$ entonces se define la {\it traza de} $A$
\begin{equation}\label{eq:c.12}
Tr(A)= \mathop{\sum}\limits_{n=1}^{\infty} \langle Ae_{n}, e_{n} \rangle ,
\end{equation}

donde $\{e_{n}\}_{n \geq 1}$ es una base ortonormal en $H$ (nuevamente tenemos que el lado derecho no depende de la base escogida).
\end{definicion}

\begin{observacion}\label{O:C.21} Si en (\ref{eq:c.12}) se toma $\{e_{n}\}_{n \geq 1}$ como los vectores propios ortonormales del operador $(A^{*} A)^{\frac{1}{2}}$ y $\{\mu _{n}\}_{n \geq 1}$ son los valores propios correspondientes, entonces tenemos 
\begin{equation}\label{eq:c.13}
\mathop{\sum}\limits_{n=1}^{\infty} \abs{\langle Ae_{n},e_{n} \rangle} \leq \mathop{\sum}\limits_{n=1}^{\infty} \mu _{n},
\end{equation}

por lo tanto la serie $\sum _{n=1}^{\infty} \langle Ae_{n},e_{n} \rangle$ es absolutamente convergente.
\end{observacion} 

\begin{observacion}\label{O:C.22} Si $A \in L_{1}(H)$ y $A \geq 0$ se tiene que 
\begin{equation} \label{eq:c.14}
\norm{A}_{1}= Tr(A)= \mathop{\sum}\limits_{n=1}^{\infty} \mu_{n} ,
\end{equation}

donde $\{\mu_{n}\}_{n \geq 1}$ son como en la Observación \ref{O:C.21}.
\end{observacion}
\begin{teo}\label{T:C.23}
\begin{enumerate}[\rm (a)]
\item $\norm{\alpha A}_{1}= \abs{\alpha} \norm{A}_{1},\hspace{4pt} \alpha \in R$
\item $\norm{A+B}_{1} \leq \norm{A}_{1} + \norm{B}_{1}.$
\item $\norm{A} \leq \norm{A}_{1}.$
\item Si $A,B \in L_{2}(H)$ entonces $AB \in L_{1}(H)$ y $\norm{AB}_{1} \leq \norm{A}_{2} \norm{B}_{2}.$

Recíprocamente, si $A \in L_{1}(H)$ entonces existen $A_{1}, A_{2} \in L_{2}(H)$ tales que $A=A_{1} A_{2}.$
\item $\norm{A}_{2} \leq \norm{A}_{1}.$
\item Si $A \in L_{1}(H)$ entonces $A^{*} \in L_{1}(H)$ y $\norm{A^{*}}_{1}=\norm{A}_{1}.$
\item Si $A \in L(H)$ y $B \in L_{1}(H)$ entonces $AB \in L_{1}(H)$ y

$$ \norm{AB}_{1} \leq \norm{A} \norm{B}_{1}.$$

Si $A \in L_{1}(H)$ y $B \in L(H),$ entonces $AB \in L_{1}(H)$ y

$$ \norm{AB}_{1} \leq \norm{A}_{1} \norm{B}.$$
\end{enumerate}
\end{teo}

\begin{teo}\label{T:C.24} El espacio $L_{1}(H)$ es un espacio de Banach separable con respecto a la norma Tr.
\end{teo}

\begin{observacion}\label{O:C.25} Puesto que se tienen las desigualdades
$$\norm{A} \leq \norm{A}_{2} \leq \norm{A}_{1},$$
entonces tenemos las siguientes inclusiones
$$ L_{1}(H) \subset L_{2}(H) \subset K(H) \subset L(H).$$

Todas las inclusiones son estrictas si $dim(H)=\infty$ y se tienen las igualdades si $dim(H)< \infty .$
\end{observacion}

\begin{observacion}\label{O:C.26} El espacio $L_{1}(H)$ (resp. $L_{2}(H)$) es la cerradura de $S(H)$ con respecto a $\norm{\cdot}_{1}$ (resp. $\norm{\cdot}_{2}$).
\end{observacion}

{\it Ejemplo de un operador nuclear.} Sean $a,b \in H .$ Definimos el operador $a \otimes b \in L(H)$ por:

\begin{equation}\label{eq:c.15}
(a \otimes b)x = a \langle b,x \rangle, \hspace{6pt} \forall x \in H.
\end{equation}

\begin{prop}\label{P:C.27}
\begin{enumerate}[(i)]
\item $a \otimes b \in L_{1}(H)$ y $Tr(a \otimes b)= \langle a,b \rangle .$
\item $(a \otimes b)^{*}= b \otimes a$ y $\norm{a \otimes b}_{2}= \norm{a} \norm{b}.$
\item $a \otimes a \geq 0,$ $a \otimes a = (a \otimes a)^{*}, a \otimes a \in L_{1}(H)$ y
\begin{equation}\label{eq:c.16}
Tr(a \otimes a)= \norm{a \otimes a}_{1}= \norm{a \otimes a}_{2}= \norm{a}^{2}.
\end{equation}
\end{enumerate}
\end{prop}



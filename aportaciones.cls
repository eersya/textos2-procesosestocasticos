\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{aportaciones}[2017/02/20 Clase para la colección Aportaciones Matemáticas - Nueva era]

\LoadClassWithOptions{book}
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}

\RequirePackage{calc}
\RequirePackage{geometry}
\RequirePackage{setspace}
\RequirePackage{multicol}
\RequirePackage{type1cm}
\RequirePackage{xcolor}
\newdimen\anchorosarivo
\newdimen\altorosarivo
\newdimen\anchocap
\DeclareRobustCommand*{\rosarivo}[1][9]{%
  \setlength{\anchorosarivo}{\paperwidth/#1}%
  \setlength{\altorosarivo}{\paperheight/#1}%
  \geometry{textwidth = \anchorosarivo * (#1-3), %
    textheight = \altorosarivo * (#1-3), %
    inner=\anchorosarivo,%
    top=\altorosarivo,%
    includefoot, includehead}%
}

\DeclareRobustCommand*{\aporta}{%
  \geometry{papersize={17cm,23cm},%
    textwidth = 125mm, %
    textheight = 170mm, %
    inner=22mm,%
    top=29mm}%
}

\RequirePackage{textcomp}

%\RequirePackage[tocindentauto]{tocstyle}
%\usetocstyle{standard}

\DeclareOption{revision}{%
  \geometry{letterpaper, margin=1.5in}%
  \setstretch{1.5}%
}

\DeclareOption{tabletV}{%
  \geometry{papersize={6in,8in}, margin=0.5in, includehead, includefoot}%
  \setstretch{1.1}%
}

\DeclareOption{tabletH}{%
  \geometry{papersize={8in,6in}, margin=0.5in, includehead,%
    includefoot}%
  \setlength\columnsep{24pt}%
  \setstretch{1.1}%
}


\newif\ifplecas

\DeclareOption{conplecas}{%
  \plecastrue
}

\DeclareOption{sinplecas}{%
  \plecasfalse
}


\DeclareOption{caphelv}{%
\def\@chapter[#1]#2{\ifnum \c@secnumdepth >\m@ne
                       \if@mainmatter
                         \refstepcounter{chapter}%
                         \typeout{\@chapapp\space\thechapter.}%
                         \addcontentsline{toc}{chapter}%
                                   {\protect\numberline{\thechapter}#1}%
                       \else
                         \addcontentsline{toc}{chapter}{#1}%
                       \fi
                    \else
                      \addcontentsline{toc}{chapter}{#1}%
                    \fi
                    \chaptermark{#1}%
                    \addtocontents{lof}{\protect\addvspace{10\p@}}%
                    \addtocontents{lot}{\protect\addvspace{10\p@}}%
                    \if@twocolumn
                      \@topnewpage[\@makechapterhead{#2}]%
                    \else
                      \@makechapterhead{#2}%
                      \@afterheading
                    \fi}
\def\@makechapterhead#1{%
  \vspace*{10\p@}%
  \setlength{\anchocap}{137.652mm}
  {\parindent \z@ \raggedleft \normalfont\sffamily
  \begin{minipage}{\anchocap}
  \raggedleft
    \ifnum \c@secnumdepth >\m@ne
      \if@mainmatter
%        \Large \@chapapp\space \thechapter
\textcolor{black!30}{%
\huge \bfseries #1\hfill% \kern5pt%
        {\fontsize{68pt}{0}\bfseries \thechapter}}%
        \par\nobreak
        \vskip 0\p@
      \fi
    \fi
    \interlinepenalty\@M
%    \huge \bfseries #1\par\nobreak
%    \vspace*{5pt}
%    \ifplecas{\textcolor{black!30}{\hrule height 3pt}}\else{}\fi
    \ifplecas{\textcolor{black}{\hrule height 1pt}}\else{}\fi
    \end{minipage}
   \vskip 60\p@
   }
  }
\def\@schapter#1{\if@twocolumn
                   \@topnewpage[\@makeschapterhead{#1}]%
                 \else
                   \@makeschapterhead{#1}%
                   \@afterheading
                 \fi}
\def\@makeschapterhead#1{%
  \vspace*{30\p@}%
  {\parindent \z@ \raggedleft
    \normalfont\sffamily
    \interlinepenalty\@M
    \huge \bfseries  #1\par\nobreak
    \vspace*{5pt}
    \hrule
    \vskip 40\p@
  }}
}

\DeclareOption{capaporta}{%
\def\@chapter[#1]#2{\ifnum \c@secnumdepth >\m@ne
                       \if@mainmatter
                         \refstepcounter{chapter}%
                         \typeout{\@chapapp\space\thechapter.}%
                         \addcontentsline{toc}{chapter}%
                                   {\protect\numberline{\thechapter}#1}%
                       \else
                         \addcontentsline{toc}{chapter}{#1}%
                       \fi
                    \else
                      \addcontentsline{toc}{chapter}{#1}%
                    \fi
                    \chaptermark{#1}%
                    \addtocontents{lof}{\protect\addvspace{10\p@}}%
                    \addtocontents{lot}{\protect\addvspace{10\p@}}%
                    \if@twocolumn
                      \@topnewpage[\@makechapterhead{#2}]%
                    \else
                      \@makechapterhead{#2}%
                      \@afterheading
                    \fi}


\def\@makechapterhead#1{%
  \vspace*{10\p@}%
  {\parindent \z@ \raggedleft \normalfont
  \ifnum \c@secnumdepth >\m@ne
  \if@mainmatter
  \huge\bfseries \scalebox{2}{\thechapter}
  \par\nobreak
  \vskip 6\p@
  \fi
  \fi
  \interlinepenalty\@M
  \huge \bfseries\itshape #1\par\nobreak
  \vskip 80\p@
  }}


%\def\@makechapterhead#1{%
%  \vspace*{10\p@}%
%  {\parindent \z@ \raggedleft \normalfont\sffamily
%    \ifnum \c@secnumdepth >\m@ne
%      \if@mainmatter
%        \Huge\bfseries\scalebox{2}{\thechapter}
%        \par\nobreak
%        \vskip 0\p@
%      \fi
%    \fi
%    \interlinepenalty\@M
%    \huge \bfseries\sffamily #1%\par\nobreak
%    \vspace*{10pt}%
%      \hrule
%   \vskip 40\p@
%   }
%  } 

\def\@schapter#1{\if@twocolumn
                   \@topnewpage[\@makeschapterhead{#1}]%
                 \else
                   \@makeschapterhead{#1}%
                   \@afterheading
                 \fi}
\def\@makeschapterhead#1{%
  \vspace*{10\p@}%
  {\parindent \z@ \raggedleft \normalfont
        \huge\bfseries\scalebox{2}{\phantom{0}}
        \par\nobreak
        \vskip 6\p@  
% 
    \interlinepenalty\@M
    \huge \bfseries\itshape  #1%\par\nobreak
    \vskip 80\p@
  }}               
%\def\@makeschapterhead#1{%
%  \vspace*{10\p@}%
%  {\parindent \z@ \raggedleft \normalfont\sffamily
%%
%        \Huge\bfseries\scalebox{2}{\phantom{0}}
%        \par\nobreak
%        \vskip 0\p@  
%% 
%    \interlinepenalty\@M
%    \huge \bfseries  #1%\par\nobreak
%    \vspace*{10pt}%
%    \hrule
%    \vskip 40\p@
%  }}
}

\def\cleardoublepage{\clearpage\if@twoside \ifodd\c@page\else
\hbox{}
\vspace*{\fill}
\thispagestyle{empty}
\newpage
\if@twocolumn\hbox{}\newpage\fi\fi\fi}

\ExecuteOptions{sinplecas,capaporta}
\ProcessOptions\relax	
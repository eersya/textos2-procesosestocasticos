\setcounter{chapter}{9}
\chapter{Integrales y derivadas fracionarias}\label{J1}

Sean $0<T< \infty$ y $0< \alpha<1$. Si $f \colon \left[ 0,T \right] \to R$, $g \colon R_{+} \to R$ entonces se definen las \textit{integrales fraccionarias de Liouville de orden $\alpha$} por:

\begin{equation}\label{eq:j.1}
\big( I^{\alpha}_{T -} f \big)(s) = \frac{1}{\Gamma (\alpha)} \int_{s}^{T} f(u)(u-s)^{\alpha -1}du, \quad s \in (0,T),
\end{equation}

\begin{equation}\label{eq:j.2}
\big( I^{\alpha}_{0 +} f \big)(s) = \frac{1}{\Gamma (\alpha)} \int_{0}^{s} f(u)(s-u)^{\alpha -1}du, \quad s \in (0,T),
\end{equation}

\begin{equation}\label{eq:j.3}
\big( I^{\alpha}_{-} g \big)(s) = \frac{1}{\Gamma (\alpha)} \int_{s}^{\infty} g(u)(u-s)^{\alpha -1}du, \quad s \in (0,\infty),
\end{equation}

Nos referimos al libro \cite{167} para un estudio bastante completo de las integrales y derivadas fraccionarias.\\
Es fácil ver que las integrales fraccionarias $I^{\alpha}_{T -} f$, $I^{\alpha}_{0 +} f$, $I^{\alpha}_{-} g$ están bien definidas para $f \in L^{1}(\left[0,T\right])$, $g \in L^{p} (R_{+})$, $p \geq 1$.\\
Si $f \in I^{\alpha}_{T-}(L^{2}(\left[0,T\right]))$ (resp. $f \in I^{\alpha}_{0+}(L^{2}(\left[0,T\right]))$), entonces la función $\varphi \in L^{2}(\left[ 0,T \right])$ tal que $I^{\alpha}_{T-} \varphi = f$ (resp. $I^{\alpha}_{0+} \varphi = f$) Es únicamente determinada y coincide con la \textit{derivada de Marchaud} de orden $\alpha$, definida para casi todo $s$, por

\begin{equation}\label{eq:j.4}
D^{\alpha}_{T-} f(s) = \frac{1}{\Gamma(1- \alpha)}\Bigg[ \frac{f(s)}{(T-s)^{\alpha}} - \alpha \int_{s}^{T} \frac{f(u)-f(s)}{(u-s)^{\alpha +1}} du \Bigg],
\end{equation}

respectivamente,

\begin{equation}\label{eq:j.5}
D^{\alpha}_{0+}f(s) = \frac{1}{\Gamma (1- \alpha)}\Bigg[ \frac{f(s)}{s^{\alpha}} + \alpha \int_{0}^{s} \frac{f(s) - f(u)}{(s-u)^{\alpha +1}}du \Bigg],
\end{equation}

o con la \textit{derivada de Riemann- Liouville}

\begin{equation}\label{eq:j.6}
\cs{D}_{T-}^{\alpha} f(s) = - \frac{1}{\Gamma(1-\alpha)} \frac{d}{ds} \int_{s}^{T} \frac{f(u)du}{(u-s)^{\alpha}} = -\frac{d}{ds}\big(I ^{1-\alpha}_{T-}f \big)(s),
\end{equation}

respectivamente,

\begin{equation}\label{eq:j.7}
\cs{D}^{\alpha}_{0+} f(s) = \frac{1}{\Gamma(1-\alpha)} \frac{d}{ds} \int_{0}^{s} \frac{f(u) du}{(s-u)^{\alpha}} = \frac{d}{ds} \big( I_{0+}^{1-\alpha} f \big)(s).
\end{equation}

Se tiene

\begin{equation}\label{eq:j.8}
I^{\alpha}_{T-} D^{\alpha}_{T-} f = f, \quad \forall  f \in I^{\alpha}_{T-}(L^{1}(\left[ 0,T \right])),
\end{equation}

\begin{equation}\label{eq:j.9}
D^{\alpha}_{T-} I^{\alpha}_{T-} f = f, \quad \forall  f \in L^{1}(\left[ 0,T \right]).
\end{equation}

Relaciones similares se cumplen para $I^{\alpha}_{0+}$ y $D^{\alpha}_{0+}$. Por eso si $\alpha<0$ a menudo se usa la notación $I^{\alpha}_{T-} = D^{-\alpha}_{T-}$.

\begin{lema}\label{J.L1}
(Lema 2.2 de \cite{167}). Si $f$ es absolutamente continua entonces $D^{\alpha}_{T-}f$, $D^{\alpha}_{0+}f \in L^{r}(\left[ 0,T \right])$ para cada $1 \leq r < \alpha^{-1}$.
\end{lema}

El lema que sigue es muy útil (ver Lema 3.2-pag. 70 y Corolario 1-pag. 208 de \cite{167}).

\begin{lema}\label{J.L2}
\begin{enumerate}[(a)]
\item Si $\varphi \in L^{p}(\left[ 0,T \right])$, $1<p< \infty$, $\mu > -1 + \frac{1}{p}$, entonces se cumplen las igualdades

\begin{equation}\label{eq:j.10}
I^{\alpha}_{0+}(x^{\mu} \varphi) = x^{\mu} I^{\alpha}_{0+}(\varphi + A_{1} \varphi),
\end{equation}

\begin{equation}\label{eq:j.11}
x^{\mu} I^{\alpha}_{0+} \varphi = I^{\alpha}_{0+}(x^{\mu}(\varphi + A_{2}\varphi)),
\end{equation}

donde $A_{i}$ son operadores continuos en $L^{p}(\left[ 0,T \right])$.

\item Si $1<p< \frac{1}{\alpha}$ entonces $I^{\alpha}_{T-}(L^{p}(\left[ 0,T \right])) = I^{\alpha}_{0+}(L^{p}(\left[ 0,T \right]))$.

\end{enumerate}
\end{lema}

\begin{lema}\label{J.L3}
(ver Corolario-pag.237 de \cite{167}). Si $\left[ 0,a \right] \subset \left[ 0,T \right]$ y $f \in I^{\alpha}_{0+}(L^{p}(\left[ 0,a \right])) = I^{\alpha}_{a-}(L^{p}(\left[ 0,a \right]))$, $1<p< \frac{1}{\alpha}$ entonces $f 1_{\left[ 0,a \right]} \in I^{\alpha}_{0+}(L^{p}(\left[ 0,T \right])) = I^{\alpha}_{T-}(L^{p}(\left[ 0,T \right]))$.
\end{lema}

\begin{lema}\label{J.L4}
Para $1 \leq p < \infty$ el operador

$$ I^{\alpha}_{T-}(x^{- \alpha}.) \colon L^{p}(\left[ 0,T \right]) \to L^{p}(\left[ 0,T \right]), $$

es continuo. En particular el operador

$$ x^{\alpha} I^{\alpha}_{T-}(x^{-\alpha}.) \colon L^{p}(\left[ 0,T \right]) \to L^{p}(\left[ 0,T \right]), $$

es continuo.
\end{lema}

\begin{proof}
Vamos a usar la siguiente desigualdad (ver \cite{167}-(5.45')),

$$ \int_{0}^{\infty} x^{-(\alpha + \beta + \gamma)p} | x^{\beta} I^{\alpha}_{-} (x^{\gamma} f_{1})(x)|^{p} dx \leq k_{p} \int_{0}^{\infty} |f_{1}(x)|^{p}dx, \quad f_{1} \in L^{p}(R_{+}), $$

para $1 \leq p < \infty \quad$ y $\quad (\alpha + \gamma)p < 1$.\\
Ahora si $f \in L^{p}(\left[ 0,T \right])$ definamos $f_{1} = f$ en $\left[ 0,T \right]$ y $f=0$ en $(T,\infty)$. Aplicando la desigualdad anterior para $\beta=0$, $\gamma=-\alpha$ se obtiene

$$ \int_{0}^{T} | I^{\alpha}_{T-} (x^{-\alpha} f) (x) |^{p} dx = \int_{0}^{\infty} | I^{\alpha}_{T-} (x^{-\alpha} f_{1}) (x) |^{p} dx \leq k_{p} \int_{0}^{T} | f(x) |^{p} dx. $$

\end{proof}


\setcounter{chapter}{3}
\chapter{Probabilidades en espacios de Hilbert}\label{D1}

Sea $(H, \langle \cdot \rangle)$ un espacio de Hilbert real y separable y sea $F \in \cs{P}_{r}(H).$

\begin{definicion}
\begin{enumerate}[\rm (a)]
\item Llamamos la {\it media} (o {\it esperanza}) de $F$ al único elemento  $E(F) \in H$ (si existe) tal que
\begin{equation}\label{eq:d.1}
\langle E(F),x \rangle = \int _{H}  \langle z,x \rangle \  dF(z), \hspace{5pt} \forall x \in H.
\end{equation}

\item Se llama la {\it covarianza} de $F$ al único operador $C_{F} \in L(H)$ (si existe) tal que
\begin{equation}\label{eq:d.2}
\langle C_{F}x,y \rangle = \int_{H} \langle x,z-E(F) \rangle \langle y, z-E(F) \rangle \  dF(z), \hspace{5pt} \forall x,y \in H
\end{equation}
\end{enumerate}
\end{definicion}

\begin{observacion}\label{D:O2} Si $C_{F}$ existe es claro que $C_{F} \geq 0,\    C_{F}=C_{F}^{*}.$
\end{observacion}

\begin{prop}\label{D:P3}
\begin{enumerate}[\rm (a)]
\item \label{PD3 (a)} Si $\int_{H} \norm{x} \  dF(x)< \infty,$ entonces $E(F)$ existe y
\begin{equation}\label{eq:d.3}
\norm{E(F)} \leq \int_{H} \norm{x} \  dF(x)
\end{equation}
\item \label{PD3 (b)} Si $\int_{H} \norm{x}^{2} \  dF(x)< \infty,$ entonces $C_{F}$ existe. Además $C_{F} \in L_{1}(H)$ y
\begin{equation}\label{eq:d.4}
Tr(C_{F})= \int_{H} \norm{x}^{2} \  dF(x)- \norm{E(F)}^{2} = \int_{H} \norm{x-E(F)}^{2} \  dF(x).
\end{equation}
\end{enumerate}

Recíprocamente, si $C_{F}$ existe y $C_{F} \in L_{1}(H),$ entonces se tendrá que

$\int_{H} \norm{x}^{2} \   dF(x)< \infty.$
\end{prop}

\begin{proof}[\bf Demostración]
\begin{enumerate}[\rm(a)]
\item\label{Dem d(a)}  La funcional $\varphi \colon H \to R$ definida como
$$\varphi(x)=\int_{H} \langle z,x \rangle \  dF(z)$$

es lineal y continua lo que implica por el teorema de Riesz que existe un único elemento $E(F) \in H$ tal que (\ref{eq:d.1}) y (\ref{eq:d.3}) se cumplen.

\item \label{Dem d(b)}Supongamos que $\int_{H} \norm{x}^{2} \  dF(x)< \infty.$ Por la desigualdad de Schwarz se tiene
$$  \int_{H} \norm{x} \  dF \leq \left[ \int_{H} \norm{x}^{2} \  dF(x) \right]^{\frac{1}{2}} < \infty$$

de donde resulta de la parte (a) que $E(F)$ existe. Como
$$ \abs{ \langle x, u \rangle \   \langle y,u \rangle} \leq  \norm{x} \Vert y \Vert   \norm{u}^{2}$$

entonces

$$ \left| \int_{H} \langle x, z-E(F) \rangle \  \langle y,z-E(F) \rangle \  dF(z) \right|$$ 

$$ \leq \norm{x}\  \Vert y \Vert  \int_{H} \norm{z-E(F)}^{2}\  dF(z)$$

$$ \leq 2 \left[ \int_{H} \norm{z}^{2}\   dF(z)+ \norm{E(F)}^{2} \right] \norm{x}\  \Vert y \Vert $$

lo que implica que la funcional bilineal $ B \colon H \times H \to R$ definida como 

$$ B(x,y)= \int_{H} \langle x, z-E(F) \rangle \langle y,z-E(F) \rangle dF(z)$$

es continua y por lo tanto existe $C_{F} \in L(H)$ tal que (\ref{eq:d.2}) se cumple.


Sea $\{e_{n}\}_{n \geq 1}$ una base ortonormal en $H$. Tenemos

$$ \mathop{\sum}\limits_{n=1}^{\infty} \langle C_{F}e_{n},e_{n} \rangle = \mathop{\sum}_{n=1}^{\infty} \int_{H} \langle x-E(F), e_{n} \rangle ^{2} dF(x)$$

$$ = \int_{H} \mathop{\sum}\limits_{n=1}^{\infty} \langle x-E(F),e_{n} \rangle ^{2} dF(x) = \int_{H} \norm{x-E(F)} ^{2} dF(x) < \infty, $$

lo 	que implica que

$$Tr(C_{F})= \int_{H} \norm{x-E(F)}^{2} dF(x).$$

Como

$$\norm{x-E(F)}^{2}= \norm{x}^{2} - 2 \langle x,E(F) \rangle + \norm{E(F)}^{2}$$

$$ \norm{E(F)}^{2}= \langle E(F), E(F) \rangle = \int_{H} \langle x, E(F) \rangle dF(x).$$

se obtiene que  

$$\int_{H} \norm{x-E(F)}^{2} dF(x) = \int_{H} \norm{x}^{2} dF(x)- 2 \norm{E(F)}^{2} + \norm{E(F)}^{2}$$


$$ = \int_{H} \norm{x}^{2} dF(x)- \norm{E(F)}^{2}.$$

Recíprocamente, supongamos que $C_{F}$ existe y $C_{F} \in L_{1}(H).$ Por definición de $C_{F}$ resulta que $E(F)$ existe.

Ahora

$$ \int_{H} \norm{x}^{2}dF(x) \leq 2 \int_{H} \norm{x-E(F)}^{2} dF(x)+2 \norm{E(F)}^{2}$$

$$ =2 \mathop{\lim}\limits_{n \to \infty} \int_{H} [ \langle x-E(F),e_{1} \rangle ^{2} +\cdots + \langle x-E(F), e_{n} \rangle ^{2}]dF(x)$$

$$+2\norm{E(F)}^{2}= 2 \mathop{\lim}\limits_{n \to \infty} \mathop{\sum}\limits_{j=1}^{n} \langle C_{F}e_{j}, e_{j} \rangle + 2\norm{E(F)}^{2}$$

$$= 2 \mathop{\sum}\limits_{j=1}^{\infty} \langle C_{F}e_{j},e_{j} \rangle + 2\norm{E(F)}^{2}= 2Tr C_{F} + 2 \norm{E(F)}^{2} < \infty .$$
\end{enumerate}
\end{proof}

\begin{definicion}\label{D:D4} Sea $f \colon \proceso \to H$ un elemento aleatorio. Se define {\it la media y la covarianza} de $f$ por

\begin{equation}\label{eq:d.5}
E(f)=E(P \circ f^{-1}),\hspace{6pt} C_{f}=C_{P \circ f^{-1}}.
\end{equation}

respectivamente, es decir, $E(f)$ es el único elemento de $H$ que satisface

\begin{equation}\label{eq:d.6}
\langle E(f),x \rangle= \int_{\Omega} \langle x,f \rangle dP,\hspace{3pt} \forall x \in H
\end{equation}

y $C_{f}$ el único operador de $L(H)$, no-negativo y autoadjunto, que satisface

\begin{equation}\label{eq:d.7}
\langle C_{f}x,y \rangle = \int_{\Omega} \langle x,f- E(f) \rangle \langle y,f-E(f) \rangle dP, \hspace{6pt} \forall x,y \in H.
\end{equation}
\end{definicion}

\begin{definicion}\label{D:D5} Definimos {\it la función característica de} F como la función $ \varphi_{F} \colon H \to C$ definida por
\begin{equation}\label{eq:d.8}
\varphi_{F}(t)= \int_{H} e^{i \langle t,x \rangle} dF(x).
\end{equation}

Si $f \colon \proceso \to H$ es un elemento aleatorio se define {\it la función característica de} f como

$$ \varphi_{f}= \varphi_{P \circ f^{-1}},$$

es decir, $\varphi_{f} \colon H \to C$ está dada por

\begin{equation}\label{eq:d.9}
\varphi_{f}(t)= \int_{H} e^{i \langle t,f \rangle}dP.
\end{equation}

Se tiene el siguiente resultado importante.
\end{definicion}

\begin{teo}\label{D:T6} (Teorema de Bochner-Minlos-Sazonov). \begin{enumerate}[\rm (a)] 
\item \label{T6 (a)} La función característica de $\varphi_{F}$ es uniformemente continua, $\varphi_{F}(0)=1$ y $\varphi_{F}$ es de tipo positivo, es decir, para cada $t_{1}, \ldots ,t_{n} \in H, z_{1}, \ldots ,z_{n} \in C$ se tiene

\begin{equation}\label{eq:d.10}
\mathop{\sum}\limits_{j,k=1}^{n} \varphi_{F}(t_{j}-t_{k})z_{j}\overline{z}_{k} \geq 0.
\end{equation}

Además para cada $\epsilon >0$ existe $S_{\epsilon} \in L_{1}(H),\   S_{\epsilon} \geq 0,\   S_{\epsilon}=S_{\epsilon}^{*}$ tal que
\begin{equation}\label{eq:d.11}
1-Re  \varphi_{F}(t) \leq \epsilon + \langle S_{\epsilon}t, t \rangle , \hspace{8pt} \forall t \in H
\end{equation}

\item \label{T6 (b)} Recíprocamente, si $\varphi \colon H \to C$ satisface las condiciones de (\ref{T6 (a)}) entonces existe $F \in \cs{P}_{r}(H)$ única tal que $\varphi= \varphi_{F}.$
\end{enumerate}
\end{teo}
\begin{proof}[\bf Demostración]
\begin{enumerate}[\rm (a)]
\item \label{Dem D6 (a)} Es claro que 

$$\varphi_{F}(0)= \int_{H} e^{i \langle 0,x \rangle} dF(x) = \int_{H} 1dF(x) = F(H)=1$$

$$ \abs{ \varphi_{F}(t+h)-\varphi_{F}(t)} \leq \int_{H} \abs{e^{i \langle t+h,x \rangle} - e^{i \langle t,x \rangle}} dF(x)$$

$$ = \int_{H} \abs{e^{i \langle h,x \rangle} - 1} dF(x).$$

Como $\mathop{\lim}\limits_{h \to 0} e^{i \langle h,x \rangle} =1$ y $\abs{e^{i \langle h,x \rangle} -1} \leq 2,$  resulta por el teorema de convergencia dominada que

$$ \mathop{\lim}\limits_{h \to 0} \int_{H} \abs{e^{i \langle h,x \rangle}-1} dF(x)=0$$

y por ende

$$ \mathop{\lim}\limits_{h \to 0} \mathop{\sup}\limits_{t \in H} \abs{ \varphi_{F}(t+h)-\varphi_{F}(t)}=0,$$

es decir $\varphi_{F}$ es uniformemente continua.

Luego tenemos que

$$\mathop{\sum}\limits_{j,k=1}^{n} \varphi_{F}(t_{j}-t_{k})z_{j}\overline{z}_{k}= \int_{H} \mathop{\sum}\limits_{j,k=1}^{n} e^{i \langle t_{j}-t_{k},x \rangle} z_{j}\overline{z}_{k} dF(x)$$

$$= \int_{H} \mathop{\sum}\limits_{j=1}^{n} e^{i \langle t_{j},x \rangle} z_{j} \left( \overline{\mathop{\sum}\limits_{k=1}^{n} e^{i \langle t_{k},x \rangle} z_{k}} \right) dF(x)$$ 

$$ = \int_{H} \left| \mathop{\sum}\limits_{j=1}^{n} e^{i \langle t_{j},x \rangle} z_{j} \right|^{2} dF(x) \geq 0.$$

Ahora definimos $S^{R} \in L(H),$ para cada $R>0,$ por 

\begin{equation}\label{eq:d.12}
\langle S^{R}x,x \rangle = \mathop{\int}\limits_{B(0,R)} \abs{ \langle x,y \rangle }^{2} dF(y)
\end{equation}

(recordemos que $B(0,R)= \{x \in H \colon  \norm{x} < R \}$).

Sea la medida  finita $G=1_{B(0,R)} \cdot F.$ Es claro que

$$\int_{H} \norm{x}^{2} dG(x)= \int_{B(0,r)} \norm{x}^{2} dF(y) \leq R^{2}$$

resulta de la Proposición (\ref{D:P3})-(\ref{PD3 (b)}) que $S^{R} \in L_{1}(H)$ y


\begin{equation}\label{eq:d.13}
Tr(S^{R})= \int_{B(0,R)} \norm{x}^{2} dF(x).
\end{equation}

Tenemos

$$ 1-Re \   \varphi_{F}(t) = \int_{H} [1- \cos \langle t,x \rangle ] dF(x)$$

$$= 2 \int_{H} \left[ \sen \frac{\langle t,x \rangle}{2} \right]^{2} dF(x) \leq 2 \mathop{\int}\limits_{B(0,R)} \left[ \sen \frac{\langle t,x \rangle}{2} \right]^{2}  dF(x)$$

$$ +2F(B(0,R)^{c}) \leq \frac{1}{2} \mathop{\int}\limits_{B(0,R)} \langle t,x \rangle ^{2} dF(x) + 2F(B(0,R)^{c})$$ 

$$= \frac{1}{2}\   \langle S^{R}t,t \rangle + 2F(B(0,R)^{c})$$

lo que muestra que (\ref{eq:d.11}) si se toma $R$ tal que $F(B(0,R)^{c})< \frac{\epsilon}{2}$  y  $S_{\epsilon}=S_{R}.$

\item \label{Dem D6(b)} Sea $\{e_{n}\}_{n \geq 1}$ una base ortonormal en $H$ y $\pi_{n}= \mathop{\sum}\limits_{j=1}^{n} e_{j} \otimes e_{j}$ la proyección ortogonal sobre $H_{n} = \Span\{e_{1}, \dots, e_{n}\}$ (el espacio lineal generado por $e_{1},\dots,e_{n}$).

Definamos $\varphi_{n} \colon R^{n} \to C$ por

$$\varphi_{n}(t_{1},\dots,t_{n})= \varphi(\langle t_{1},e_{1} \rangle e_{1} + \cdots + \langle t_{n}, e_{n} \rangle e_{n}).$$

Como $\varphi_{n}$ satisface las hipótesis del teorema de Bochner en dimensión finita entonces existe $F_{n} \in \cs{P}_{r}(R^{n})$ tal que $\varphi_{n}=\varphi_{F_{n}}.$ Sea $p_{n}^{n+1} \colon R^{n+1} \to R^{n}$ la proyección canónica. Probaremos que 

\begin{equation}\label{eq:d.14}
F_{n+1} \circ \left[ p_{n}^{n+1} \right]^{-1}=F_{n}, \hspace{8pt} \forall n \geq 1
\end{equation}

Para ver esto probaremos la igualdad de las funciones características.

Tenemos

$$\varphi_{F_{n+1} \circ \left[ p_{n}^{n+1} \right]^{-1}} (t_{1}, \dots, t_{n})= \varphi_{F_{n+1}} (t_{1}, \dots, t_{n},0)$$

$$ = \varphi( \langle t_{1},e_{1} \rangle \  e_{1} + \cdots + \langle t_{n}, e_{n} \rangle \  e_{n} + \langle 0, e_{n+1} \rangle \  e_{n+1} )$$

$$= \varphi_{F_{n}}(t_{1}, \dots, t_{n} ).$$

A continuación para cada $J=\{n_{1}, \dots, n_{k} \} \subset \{1, \dots , n, \dots \},$  con $ n_{1} < n_{2} < \cdots < n_{k} ,$ definimos la probabilidad $P_{J}$ sobre $ \cs{B}(R^{k})$ por
\begin{equation} \label{eq:d.15}
P_{J}=F_{n_{k}} \circ \left[ p_{J}^{n_{k}} \right]^{-1}, 
\end{equation}

donde $p_{J}^{n_{k}} \colon  R^{n_{k}} \to R^{k}$  está definida por $(p_{J}^{n_{k}})((x_{j})_{1 \leq j \leq n_{k}})= (x_{j})_{j \in J}.$  Usando (\ref{eq:d.14}) se ve fácilmente que la familia $\{P_{J}\}_{J \subset \{ 1, \dots , n, \dots \} , J\   \textit{finito}}$  es consistente. Por el teorema %teorema 1.3.7.% 
existe un espacio de probabilidad $\proceso$ y una sucesión de variables aleatorias $X_{n} \colon \proceso \to R$ tal que 

$$ P \circ (X_{1}, \dots , X_{n})^{-1}= F_{n}, \hspace{8pt} \forall n \geq 1,$$

lo que es equivalente a 

$$\varphi_{(X_{1}, \dots , X_{n})} (t_{1}, \dots, t_{n}) := \int _{R^{n}} e^{i (t_{1}X_{1}+ \cdots + t_{n}X_{n})} dP$$

\begin{equation}\label{eq:d.16}
= \varphi ( \langle t_{1},e_{1} \rangle \  e_{1} + \cdots + \langle t_{n}, e_{n} \rangle \  e_{n}), \hspace{8pt} \forall t_{j} \in R.
\end{equation}

Probaremos ahora que la variable aleatoria

\begin{equation}\label{eq:d.17}
X= \mathop{\sum}\limits_{n=1}^{\infty} X_{n}^{2} < \infty \hspace{7pt} \textit{c.s..}
\end{equation}

Sea $N(0,I_{n})$ la distribución normal sobre $R^{n}$ de media $0$ y covarianza $I_{n}$.

Recordemos que la función característica de $N(0,I_{n})$ es igual a
$$\varphi_{N(0,I_{n})}(t_{1}, \dots , t_{n})= \int_{R^{n}} e^{i(t_{1}y_{1}+ \cdots + t_{n}y_{n})} dN(0,I_{n})(y_{1}, \dots, y_{n})$$

\begin{equation}\label{eq:d.18}
=e^{- \frac{1}{2} (t_{1}^{2} + \dots + t_{n}^{2})}
\end{equation} 

Para $\epsilon >0$ sea $S_{\epsilon} \in L_{1}(H)$ tal que 
\begin{equation}\label{eq:d.19}
1- Re\  \varphi(t) \leq \epsilon + \langle S_{\epsilon}t,t \rangle .
\end{equation}

Usando (\ref{eq:d.18}) y el teorema de Fubini se obtiene

$$\int_{\Omega} e^{ - \frac{1}{2} (X_{k+1}^{2} + \cdots + X_{k+n}^{2})} dP = \int_{\Omega} \left\lbrace \int_{R^{n}} e^{ i \mathop{\sum}\limits_{j=1}^{n} X_{k+j} y_{j}} dN(0,I_{n})(y_{1}, \dots, y_{n}) \right\rbrace dP$$

$$= \int_{R^{n}} \left\lbrace \int_{\Omega} e^{ i \mathop{\sum}\limits_{j=1}^{n} X_{k+j} y_{j}} dP \right\rbrace dN (0, I_{n})(y_{1}, \dots , y_{n})$$  

$$= \int_{R^{n}} \varphi(y_{1} e_{k+1} + \cdots + y_{n} e_{k+n}) dN(0,I_{n})(y_{1}, \dots , y_{n})$$

$$ = \int_{R^{n}} Re \  \varphi(y_{1} e_{k+1} + \cdots + y_{n} e_{k+n}) dN(0,I_{n})(y_{1}, \dots , y_{n})$$

y por tanto, usando (\ref{eq:d.19}), se deduce que

$$1- \int_{\Omega} e^{ - \frac{1}{2} (X_{k+1}^{2} + \cdots + X_{k+n}^{2})} dP$$

$$ = \int_{R^{n}} [ 1 - Re \varphi (y_{1}e_{k+1} + \cdots + y_{n}e_{k+n})] dN(0,I_{n}) (y_{1}, \dots , y_{n})$$

$$\leq \int_{R^{n}} [ \langle S_{\epsilon} (y_{1}e_{k+1} +  \cdots + y_{n}e_{k+n}) , y_{1} e_{k+1} + \cdots$$ 

$$ \cdots + y_{n} e_{k+n} \rangle + \epsilon ] dN(0,I_{n})(y_{1}, \dots , y_{n}) $$

$$\leq \epsilon + \int_{R^{n}} \mathop{\sum}\limits_{j,l=1}^{n} y_{j}y_{l} \langle S_{\epsilon} e_{k+j}, e_{k+l} \rangle dN(0,I_{n}) (y_{1}, \dots, y_{n})$$

$$= \epsilon + \mathop{\sum}\limits_{j,l=1}^{n} \langle S_{\epsilon} e_{k+j}, e_{k+l} \rangle \int_{R^{n}} y_{j}y_{l}dN(0,I_{n})(y_{1}, \dots , y_{n})$$

$$= \epsilon + \mathop{\sum}\limits_{j=1}^{n} \langle S_{\epsilon} e_{k+j},e_{k+j} \rangle .$$

Haciendo $  n \to \infty$ en la desigualdad anterior se obtiene

\begin{equation}\label{eq:d.20}
1- \int_{\Omega} e^{ - \frac{1}{2} \mathop{\sum}\limits_{j=1}^{\infty} X_{k+j}^{2}}dP \leq \epsilon + \mathop{\sum}\limits_{m=k+1}^{\infty} \langle S_{\epsilon} e_{m},e_{m} \rangle \leq 2\epsilon, \hspace{8pt}  \forall k \geq k_{0},
\end{equation}

debido a que la serie $\mathop{\sum}\limits_{n=1}^{\infty} \langle S_{\epsilon}e_{n}, e_{n} \rangle < \infty ( S_{\epsilon} \in L_{1}(H)).$

Por lo tanto (\ref{eq:d.20}) da a entender que

$$\int_{\Omega} e^{- \frac{1}{2} \mathop{\sum}\limits_{j=1}^{\infty} X_{k+j}^{2}} dP \geq 1-2 \epsilon , \hspace{9pt} \forall k \geq 0.$$

Por último se tiene que 

$$ P(X< \infty) \geq \int_{X < \infty} e^{- \frac{1}{2} \mathop{\sum}\limits_{j=1}^{\infty} X_{k+j}^{2}} dP$$

$$= \int_{\Omega} e^{- \frac{1}{2} \mathop{\sum}\limits_{j=1}^{\infty} X_{k+j}^{2}} dP \geq 1-2 \epsilon,$$

de donde resulta haciendo $\epsilon \to \infty$ que $P(X< \infty)=1,$ es decir (\ref{eq:d.17}) se cumple.

Sin pérdida de generalidad se puede suponer que $X< \infty$ en todo $\Omega$. La (\ref{eq:d.17}) permite definir el elemento aleatorio 

$$Y= \mathop{\sum}\limits_{n=1}^{\infty} X_{n}e_{n} \colon \proceso \to (H, \cs{B}(H)).$$

Definimos la probabilidad $F \in \cs{P}_{r}(H)$ por $F= P \circ Y^{-1}$. Entonces tenemos

$$\pi_{n}(Y)= \mathop{\sum}\limits_{k=1}^{n} X_{k}e_{k}$$

y por (\ref{eq:d.16}) tenemos 

\begin{equation}\label{eq:d.21}
\varphi(\pi_{n}t)= \varphi_{(X_{1}, \dots , X_{n})}(t) = \int_{\Omega} e^{i \langle t, \pi_{n}Y \rangle} dP
\end{equation}

Y como $\pi_{n}t \mathop{\to}\limits_{n \to \infty} t $ y $ \varphi$ es una función característica, la relación (\ref{eq:d.21}) implica que en el límite se tiene

$$\varphi(t) = \int_{\Omega} e^{i \langle t, Y \rangle} dP= \varphi_{F}(t),$$

lo cual es la conclusión del teorema.
\end{enumerate}
\end{proof}

\begin{observacion}\label{D:O7} En lugar de usar el Teorema
\ref{teo3.7}
se podría utilizar la siguiente variante de la demostración.
\end{observacion}

Sea $F_{n} \in \cs{P}_{r}( Sp \{e_{1}, \dots , e_{n}\}) \subset \cs{P}_{r}(H)$ (la inclusión se debe a la inclusión continua $Sp \{e_{1}, \dots , e_{n}\} \hookrightarrow H$) tal que $\varphi_{F_{n}}(t)= \varphi(\pi_{n}t), \hspace{6pt} \forall t \in H$ (por el Teorema de Bochner finito dimensional). Entonces usando el Corolario \ref{B:C15} se puede demostrar que $\{F_{n}\}_{n \geq 1}$ es relativamente compacta en $\cs{P}_{r}(H)$ (véase Da Prato-Zabczyk \cite{23}) y por lo tanto existen una subsucesión $\{F_{n_{j}}\}_{j \geq 1}$ tal que $F_{n_{j}} \mathop{\Longrightarrow}\limits_{j \to \infty} G.$ Entonces es fácil ver que $\varphi_{G}=\varphi.$   

Vamos a ver ahora que para sumas de variables aleatorias independientes con valores en un espacio de Hilbert (en general en un espacio de Banach separable) la convergencia {\it c.s} y la convergencia {\it en probabilidad} son equivalentes.

\begin{teo}\label{D:T8} Sea $f_{1}, \ldots , f_{n}, \ldots ,$ una sucesión de variables aleatorias independientes con valores en $H$ ( $H$ puede ser un espacio de Banach separable) tal que $S_{N} := f_{1} + \dotsb + f_{N} \mathop{\rightarrow}\limits^{p} S,$ es decir,

$$ \mathop{\lim}\limits_{N \to \infty} P ( \norm{S_{N} - S} > \epsilon) =0, \hspace{6pt}  \forall \epsilon >0.$$


Entonces 

$$ \mathop{\lim}\limits_{N \to \infty} S_{N} = S = \mathop{\sum}\limits_{n=1}^{\infty} f_{n} \hspace{9pt} \   \textit{c.s..}$$


Necesitamos el siguiente lema.
\end{teo}

\begin{lema}\label{D:L9} (Estimación de Ottaviani). Sean $f_{1},  \dots ,f_{n}$ variables aleatorias independientes con valores en el espacio de Hilbert separable $H$ (o en un espacio de Banach separable) tal que

\begin{equation}\label{eq:d.22}
P \left( \mathop{\max}\limits_{j \leq n} \Vert S_{n}- S_{j} \Vert > \alpha \right) \leq c  
\end{equation}

para algunas $\alpha >0,\   c<1.$ Entonces
\begin{equation}\label{eq:d.23}
P \left( \mathop{\max}\limits_{j \leq n} \Vert S_{j} \Vert \geq 2 \alpha \right) \leq (1-c)^{-1}\   P (\norm{S_{n}} \geq \alpha).
\end{equation}
\end{lema}

\begin{proof}[\bf Demostración] Sea la variable aleatoria $m(\omega)$ definida por 

$$m(\omega)= \max \{j \leq n ;\  \Vert S_{j}(\omega) \Vert \geq 2 \alpha \}$$

(por definición $m(\omega)=n+1$ si $\{ j \leq n :\   \Vert S_{j}(\omega) \Vert \geq 2 \alpha \}= \emptyset).$ 

Entonces

$$ P( \norm{S_{n}} \geq \alpha ) \geq P \left( \norm{S_{n}} \geq \alpha , \mathop{\max}\limits_{j \leq n} \Vert S_{j} \Vert \geq 2 \alpha \right)$$

$$ = \mathop{\sum}\limits_{j=1}^{n} P( \norm{S_{n}} \geq \alpha ,\   m=j) \geq  \mathop{\sum}\limits_{j=1}^{n} P ( \Vert S_{n}- S_{j} \Vert \geq \alpha ,\   m=j)$$

porque si $m=j$ se tiene que $\Vert S_{j} \Vert \geq 2\alpha$  que junto con $ \Vert S_{n}- S_{j} \Vert \leq \alpha$ implica que $\norm{S_{n}} \geq \alpha . $

Ya que $ \Vert S_{n}-S_{j} \Vert$ es $ \cs{B}(f_{j+1}, \dots , f_{n})$-medible y $\{m=j\} \in \cs{B}(f_{1}, \dots , f_{j})$ resulta que $\Vert S_{n}-S_{j} \Vert$ es independiente de $\{m=j\}$ y como resultado se tiene que 

$$ \mathop{\sum}\limits_{j=1}^{n} P( \Vert S_{n}-S{j} \Vert \leq \alpha,\  m=j) = \mathop{\sum}\limits_{j=1}^{n} P (\norm{S_{n}} \leq \alpha)\   P(m=j)$$ 

$$ \geq (1-c) \mathop{\sum}\limits_{j=1}^{n} P (m=j)= (1-c)\   P \   \left( \mathop{\max}\limits_{j \leq n} \Vert S_{j} \Vert \geq 2 \alpha \right).$$
\end{proof}

\begin{proof}[\bf Demostración del Teorema D8] Sea $c= \frac{1}{2}$ y  $0 <\   \epsilon <\   1.$

Escojamos $N$ suficientemente grande tal que 

$$ P \left( \Vert S_{n}-S_{j} \Vert \geq \frac{\epsilon}{2} \right) < \frac{\epsilon}{2}, \hspace{7pt} \forall n > N.$$

Aplicamos el lema anterior para $\alpha = \frac{\epsilon}{2}$ y $(f_{k})_{k \geq N+1}.$  Resulta que para todo $n \geq N,$

$$P \left( \mathop{\max}\limits_{N \leq j \leq n} \Vert S_{j}-S_{N} \Vert \geq \epsilon \right) \leq  \epsilon,$$

de donde haciendo $ n \to \infty$,  $\epsilon \to 0$, $ N \to \infty$ se obtiene que $S_{N}$ es {\it c.s.} convergente.
\end{proof}

\begin{observacion}\label{D:O10} De hecho el teorema anterior se puede extender de la siguiente manera (ver por ejemplo Dudley \cite{39}-pág.251).
\end{observacion}

\begin{teo}\label{D:T11} Sean $f_{1}, \dots , f_{n}, \dots , $ variables aleatorias independientes con valores en un espacio de Banach separable y sea $S_{n}= f_{1} + \cdots + f_{n}.$

Entonces las siguientes condiciones son equivalentes:

\begin{enumerate}[(a)]

\item \label{D:T11 a)} $S_{n}$ converge {\it c.s..}

\item \label{D:T11 b)} $S_{n}$ converge en probabilidad.

\item \label{D:T11 c)} $S_{n}$ converge en distribución.
\end{enumerate}
\end{teo}
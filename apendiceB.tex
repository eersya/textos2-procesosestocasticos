\setcounter{chapter}{1}
\chapter{La convergencia débil en espacios métricos}\label{B1}

Recordaremos en esta sección algunos resultados importantes sobre la convergencia débil de probabilidades en espacios métricos (el lector puede consultar \cite{9},\cite{39} y \cite{150}).

Sean $I,\  F$ dos conjuntos y para cada $i\in I$ sea $(F_{i},\cs{T}_{i})$ un espacio topológico, y una aplicación $f_{i}\colon F\to F_{i}.$ Denotemos por $\cs{T}(f_{i}:i\in I)$ a la topología mínima sobre $F$ tal que las aplicaciones $f_{i}$ son continuas.

Hagamos a continuación las siguientes notaciones:

\vspace{8pt}
$(E,d)$\hspace{5pt}: un espacio métrico

$\cs{M}(E)$\hspace{5pt}: la familia de las medidas finitas y no negativas sobre $\cs{B}(E).$

$\cs{M}_{a}(E)$\hspace{5pt}: la familia de todas las $\alpha\in \cs{M}(E)$ con $\alpha(E)\leq a.$

$\cs{P}_{r}(E)$\hspace{5pt}: la familia de todas probabilidades sobre $\cs{B}(E).$

Es claro que $\cs{P}_{r}(E)\subset \cs{M}_{1}(E)\subset \cs{M}(E).$

Si $f\in C_{b}(E)$ definimos $\varphi_{f}\colon \cs{M}(E)\to R$ por $\varphi_{f}(\alpha)=\int_{E} fd\alpha.$

\begin{definicion}\label{B:D1} A la topología $\cs{T}(\varphi_{f}:\  f\in C_{b}(E))$ sobre $\cs{M}(E)$ se le llama {\it la topología débil}.
\end{definicion}

\begin{observacion}\label{B:O2} Siempre $\cs{M}(E)$ (en particular $\cs{M}_{a}(E),\  \cs{P}_{r}(E))$ estará dotado con la topología débil.
\end{observacion} 

\begin{observacion}\label{B:O3} Sean $\alpha_{n}, \  \alpha\in \cs{M}(E).$ Entonces $\alpha_{n}$ converge débilmente a $\alpha$ (en notación $\alpha_{n}\mathop{\Longrightarrow}\limits_{n\to \infty}\    \alpha$) si


\begin{equation*}
 \int fd\alpha_{n}\  \mathop{\longrightarrow}\limits_{n\to \infty} \int fd\alpha ,\  \forall f\in C_{b}(E).
\end{equation*} 
\end{observacion}

\begin{observacion}\label{B:O4} EL límite débil es único, es decir si $\alpha_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha$  y $\alpha_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \beta$  entonces  $\alpha = \beta.$
\end{observacion}

Se tienen las siguientes descripciones de la convergencia débil.


\begin{teo}\label{B:T5} Sean $\alpha_{n}, \alpha\in \cs{M}(E).$ Las siguientes condiciones son equivalentes:

\begin{enumerate}[\rm(a)]
\item $\alpha_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha .$
\item $\mathop{\lim}\limits_{n\to \infty} \int fd\alpha_{n}= \int fd\alpha ,$ para toda $f\colon E \to R$ uniformemente continua y acotada (como notación $f\in U_{b}(E)).$
\item $\mathop{\lim}\limits_{n\to \infty} \alpha _{n}(E)=\alpha (E)$ y  $\alpha(F)\geq \mathop{\varlimsup}\limits_{n\to \infty} \alpha_{n}(F)$ para cada conjunto cerrado $F.$
\item $\mathop{\lim}\limits_{n\to \infty} \alpha_{n}(E)=\alpha (E)$ y $\alpha (G) \leq \mathop{\varliminf}\limits_{n\to \infty}  \alpha_{n} (G)$ para cada conjunto abierto $G.$
\item $\mathop{\lim}\limits_{n\to \infty} \alpha_{n}(A)= \alpha (A),$ para toda $A\in \cs{B}(E)$ con $\alpha (\partial A)=0\  (\partial A= \bar{A}  \setminus \mathop{A}\limits^{\circ}$ es la frontera de $A$).
\end{enumerate}
\end{teo}

\begin{prop}\label{B:P6} Se tiene\   $\alpha_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha$ si y sólo si cada subsucesión contiene una subsucesión que converge débilmente a $\alpha.$
\end{prop}

\begin{prop}\label{B:P7} Sean $E,E',$ dos espacios métricos separables.
Sean $h\colon E \to E'$ una función continua y $\alpha_{n},\  \alpha\in \cs{M}(E),\   \alpha'_{n},\  \alpha' \in \cs{M}(E').$
 \begin{enumerate}[\rm(a)]
 \item Si $\alpha_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha$ en $\cs{M}(E)$ entonces $\alpha_{n} \circ h^{-1} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha \circ h^{-1}$ en $\cs{M}(E').$ 
 \item Se tiene $\alpha_{n} \otimes \alpha'_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha \otimes \alpha'$ si y sólo si $\alpha_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha$ y $\alpha'_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha' .$ Además si $E=E'=R^d$ y $\alpha_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha,\  \alpha'_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha'$ entonces $\alpha_{n} \ast \alpha'_{n} \mathop{\Longrightarrow}\limits_{n\to \infty} \alpha \ast \alpha'.$  
 \end{enumerate}
 
Recordemos que si $\alpha_{1}, \alpha_{2} \in \cs{M}(R^d)$ se define la convolución $\alpha_{1} \ast \alpha_{2} \in \cs{M}(R^d)$ como:

$$(\alpha_{1} \ast \alpha_{2})(A)= \mathop{\int}\limits_{R^d} \alpha_{1}(A-x)d \alpha_{2} (x).$$ 
\end{prop}

\begin{definicion}\label{B:D8} Si $X_{n},\  X\colon \proceso \to (E,\cs{B}(E))$ son aplicaciones medibles (se dice también que son {\it elementos aleatorios}) diremos que $X_{n}$ converge en {\it distribución} a $X$ (como notación $X_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^{d} X)$ si

$$P \circ X_{n}^{-1} \mathop{\Longrightarrow}\limits_{n\to \infty} P\circ X^{-1} \textrm{en} \  \cs{P}_{r}(E) ,$$

es decir, si

$$\int \varphi(X_{n}) dP \mathop{\longrightarrow}\limits_{n\to \infty} \int \varphi(X)dP,\hspace{9pt} \forall \varphi \in C_{b}(E).$$

El teorema \ref{B:T5} se traduce en este caso de la siguiente manera:
\end{definicion} 

\begin{teo}\label{B:T9} Sean $X_{n},\  X \colon \proceso \to (E,\cs{B}(E))$ elementos aleatorios.
Las siguientes condiciones son equivalentes:

\begin{enumerate}[\rm(a)]
\item $X_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^d X.$
\item $\int u(X_{n})dP \mathop{\longrightarrow}\limits_{n\to \infty} \int u(X)dP,\  \forall u \in U_{b}(E).$
\item $P(X \in F)\geq \mathop{\varlimsup}\limits_{n\to \infty} P(X_{n} \in F),$ para cada conjunto cerrado $F.$
\item $P(X \in G) \leq \mathop{\varliminf}\limits_{n\to \infty} P(X_{n} \in G),$ para cada conjunto abierto $G.$
\item $\mathop{\lim}\limits_{n\to \infty} P(X_{n} \in A)=P(X\in A),$ para cada conjunto de Borel $A \subset E,$ tal que $P(X \in \partial A)=0.$ 
\end{enumerate}
\end{teo}

\begin{definicion}\label{B:D10} Sea $E$ un espacio métrico separable y sean $X_{n}, X \colon \proceso \to (E,\cs{B}(E))$ elementos aleatorios. Diremos que $X_{n}$ {\it converge en probabilidad} a $X$ (en notación $X_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^p X$) si $d(X_{n},X) \mathop{\longrightarrow}\limits_{n\to \infty}^p 0,$ es decir, si 
$$\mathop{\lim}\limits_{n\to \infty} P(d(X_{n},X)> \epsilon)=0,\  \forall \epsilon >0.$$ 
\end{definicion}

\begin{prop}\label{B:P11}\begin{enumerate}[\rm(a)] \item Si $X_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^p X$ entonces $X_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^d X.$ Recíprocamente si $X_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^d a,\   a\in E,$ entonces $X_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^p a.$
\item Si $X_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^d X$ y $d(X_{n},Y_{n}) \mathop{\longrightarrow}\limits_{n\to \infty}^p 0$ entonces $Y_{n} \mathop{\longrightarrow}\limits_{n\to \infty}^d X.$

Si $f\colon E \to R$ hagamos

$$ \norm{f}_{\infty}= \mathop{\sup}\limits_{x\in E} \abs{f(x)}, \norm{f}_{L}=\left\{\frac{\abs{f(x)-f(y)}}{d(x,y)} :\  x\neq y \right 
\}, $$

 $$ \norm{f}_{BL}=\max (\norm{f}_{\infty}, \norm{f}_{L}),$$
 
 $$d_{BL}(\mu,\nu)=\sup \left\{ \left|\int fd \mu - \int fd \nu \right| ; \norm{f}_{BL} \leq 1 \right\},\  \mu , \nu \in \cs{M}(E).$$ 
\end{enumerate}
\end{prop}

\begin{teo}\label{B:T12}\begin{enumerate}[\rm(a)] \item $d_{BL}$ es una métrica sobre $\cs{M}(E)$ que genera la topología débil.
\item Si $E$ es un espacio métrico separable entonces la topología débil sobre $\cs{M}(E)$ es separable. Además si $\Lambda \subset E$ es un conjunto numerable denso en $E$ entonces la familia numerable 

$$ \Gamma= \{ \alpha= \mathop{\sum}\limits_{i=1}^k a_{i}\epsilon_{x_{i}} : k\geq 1, a_{i}\in Q_{+}, x_{i}\in \Lambda \}$$

es densa en $\cs{M}(E).$
\item Si $E$ es un espacio métrico compacto entonces $\cs{M}_{a}(E)$ es también métrico compacto. En particular si $E$ es métrico compacto entonces $\cs{P}_{r}(E)$ es un espacio métrico compacto. 
\item Si $E$ es separable y completo (es decir un espacio polaco) entonces el espacio $(\cs{M}(E),d_{BL})$ es polaco.
\end{enumerate}
\end{teo}

\begin{definicion}\label{B:D13} Una familia $\cs{M} \subset \cs{M}(E)$ es {\it uniformemente tensa} si para todo $\epsilon >0$ existe un conjunto compacto $K_{\epsilon} \subset E$ tal que 
$$ \mathop{\sup}\limits_{\alpha \in \cs{M}} \alpha (E \setminus K_{\epsilon}) \leq \epsilon.$$

Se tiene el siguiente resultado básico.
\end{definicion}

\begin{teo}\label{B:T14}{\it (Teorema de Prohorov)}. Sea $E$ un espacio métrico separable y una familia $\cs{M} \subset \cs{M} _{a},\  a>0.$
\begin{enumerate}[(1)]
\item Si $\cs{M}$ es uniformemente tensa, entonces $\cs{M}$ es relativamente compacta en la topología débil, es decir, cada sucesión de $\cs{M}$ contiene una subsucesión que converge débilmente.

\item Recíprocamente, si $E$ es un espacio polaco y $\cs{M}$ es relativamente compacta entonces $\cs{M}$ es uniformemente tensa.
\end{enumerate}
\end{teo}

\begin{coro}\label{B:C15} Una familia $\cs{M} \subset \cs{M}_{a}(E)$ es uniformemente tensa si y sólo si para cada $\epsilon >0$ y $r>0$ existe una familia finita de bolas $ \{\overline{B} (a_{i},r)\}_{1 \leq i \leq m}$ tal que
$$ \mathop{\inf}\limits_{\alpha \in M} \alpha \left( \mathop{\bigcup}\limits_{i=1}^m \overline{B} (a_{i},r) \right) \geq 1- \epsilon$$
\end{coro}

\begin{coro}\label{B:C16} Sea $E$ un espacio polaco y sean $P_{n},\  P \in \cs{P}_{r}(E)$ tales que $P_{n} \mathop{\Longrightarrow}\limits_{n \to \infty} P.$

Si $F \subset C_{b}^{.}(E)$ es una familia de funciones uniformemente acotadas y equicontinuas, entonces

$$ \mathop{\lim}\limits_{n \to \infty}\   \mathop{\sup}\limits_{f \in F} \left| \int f dP_{n} - \int f dP \right| =0.$$
\end{coro} 

Existe el siguiente teorema de Skorohod muy útil que reduce en un sentido la convergencia débil a la convergencia casi segura (ver \cite{23}, \cite{84} o \cite{161}).

\begin{teo}\label{B:T17} (La representación de Skorohod de la convergencia débil).
Sea $E$ un espacio polaco y $P_{n}, P \in \cs{P}_{r}(E).$

Si $P_{n} \mathop{\Longrightarrow}\limits_{n \to \infty} P$ entonces existen los elementos aleatorios 

$$X_{n},X \colon ([0,1],\cs{B}([0,1]),\lambda) \to (E,\cs{B}(E))$$

($\lambda$ es la medida de Lebesgue) tales que:
\begin{enumerate}[\rm(a)]
\item $P \circ X_{n}^{-1}= P_{n}$ para cada n.
\item $X_{n}$ converge casi seguramente a $X$ (en notación $X_{n} \mathop{\longrightarrow}\limits_{n \to \infty}^{c.s} X).$
\end{enumerate}
\end{teo}